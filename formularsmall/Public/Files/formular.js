var eventHandlers = {},
	subTableConditions;

var randData = [
{"kon":"Kvinna","alder":"18-24","pulsbredd":"0,5","duration":"4","stromstyrka":"800","frekvens":"70","laddning":"224","energi":"45"},
{"kon":"Kvinna","alder":"25-29","pulsbredd":"0,5","duration":"4","stromstyrka":"800","frekvens":"70","laddning":"224","energi":"45"},
{"kon":"Kvinna","alder":"30-34","pulsbredd":"0,5","duration":"6,5","stromstyrka":"800","frekvens":"50","laddning":"260","energi":"50"},
{"kon":"Kvinna","alder":"35-39","pulsbredd":"0,5","duration":"6","stromstyrka":"800","frekvens":"60","laddning":"288","energi":"55"},
{"kon":"Kvinna","alder":"40-44","pulsbredd":"0,5","duration":"6","stromstyrka":"800","frekvens":"60","laddning":"288","energi":"55"},
{"kon":"Kvinna","alder":"45-49","pulsbredd":"0,5","duration":"6","stromstyrka":"800","frekvens":"70","laddning":"336","energi":"65"},
{"kon":"Kvinna","alder":"50-54","pulsbredd":"0,5","duration":"6","stromstyrka":"800","frekvens":"70","laddning":"336","energi":"65"},
{"kon":"Kvinna","alder":"55-59","pulsbredd":"0,5","duration":"6","stromstyrka":"800","frekvens":"70","laddning":"336","energi":"65"},
{"kon":"Kvinna","alder":"60-64","pulsbredd":"0,5","duration":"6","stromstyrka":"800","frekvens":"80","laddning":"384","energi":"75"},
{"kon":"Kvinna","alder":"65-69","pulsbredd":"0,5","duration":"6,5","stromstyrka":"800","frekvens":"70","laddning":"364","energi":"70"},
{"kon":"Kvinna","alder":"70-74","pulsbredd":"0,5","duration":"6","stromstyrka":"800","frekvens":"80","laddning":"384","energi":"75"},
{"kon":"Kvinna","alder":"75-79","pulsbredd":"0,5","duration":"6,5","stromstyrka":"800","frekvens":"80","laddning":"416","energi":"80"},
{"kon":"Kvinna","alder":"80-84","pulsbredd":"0,5","duration":"6,5","stromstyrka":"800","frekvens":"80","laddning":"416","energi":"80"},
{"kon":"Man","alder":"18-24","pulsbredd":"0,5","duration":"6","stromstyrka":"800","frekvens":"50","laddning":"240","energi":"50"},
{"kon":"Man","alder":"25-29","pulsbredd":"0,5","duration":"6,5","stromstyrka":"800","frekvens":"50","laddning":"260","energi":"50"},
{"kon":"Man","alder":"30-34","pulsbredd":"0,5","duration":"6","stromstyrka":"800","frekvens":"60","laddning":"288","energi":"55"},
{"kon":"Man","alder":"35-39","pulsbredd":"0,5","duration":"6","stromstyrka":"800","frekvens":"60","laddning":"288","energi":"55"},
{"kon":"Man","alder":"40-44","pulsbredd":"0,5","duration":"6,5","stromstyrka":"800","frekvens":"60","laddning":"312","energi":"65"},
{"kon":"Man","alder":"45-49","pulsbredd":"0,5","duration":"6","stromstyrka":"800","frekvens":"70","laddning":"336","energi":"65"},
{"kon":"Man","alder":"50-54","pulsbredd":"0,5","duration":"6,5","stromstyrka":"800","frekvens":"70","laddning":"364","energi":"70"},
{"kon":"Man","alder":"55-59","pulsbredd":"0,5","duration":"6,5","stromstyrka":"800","frekvens":"70","laddning":"364","energi":"70"},
{"kon":"Man","alder":"60-64","pulsbredd":"0,5","duration":"6","stromstyrka":"800","frekvens":"80","laddning":"384","energi":"80"},
{"kon":"Man","alder":"65-69","pulsbredd":"0,5","duration":"6,5","stromstyrka":"800","frekvens":"80","laddning":"416","energi":"80"},
{"kon":"Man","alder":"70-74","pulsbredd":"0,5","duration":"7,5","stromstyrka":"800","frekvens":"70","laddning":"420","energi":"85"},
{"kon":"Man","alder":"75-79","pulsbredd":"0,5","duration":"8","stromstyrka":"800","frekvens":"70","laddning":"448","energi":"90"},
{"kon":"Man","alder":"80-84","pulsbredd":"0,5","duration":"7,5","stromstyrka":"800","frekvens":"70","laddning":"420","energi":"85"},
{"kon":"Kvinna","alder":"18-24","pulsbredd":"1","duration":"3","stromstyrka":"800","frekvens":"50","laddning":"240","energi":"45"},
{"kon":"Kvinna","alder":"25-29","pulsbredd":"1","duration":"3","stromstyrka":"800","frekvens":"50","laddning":"240","energi":"45"},
{"kon":"Kvinna","alder":"30-34","pulsbredd":"1","duration":"3","stromstyrka":"800","frekvens":"50","laddning":"240","energi":"50"},
{"kon":"Kvinna","alder":"35-39","pulsbredd":"1","duration":"2,5","stromstyrka":"800","frekvens":"70","laddning":"280","energi":"60"},
{"kon":"Kvinna","alder":"40-44","pulsbredd":"1","duration":"2,5","stromstyrka":"800","frekvens":"70","laddning":"280","energi":"60"},
{"kon":"Kvinna","alder":"45-49","pulsbredd":"1","duration":"3","stromstyrka":"800","frekvens":"70","laddning":"336","energi":"65"},
{"kon":"Kvinna","alder":"50-54","pulsbredd":"1","duration":"3","stromstyrka":"800","frekvens":"70","laddning":"336","energi":"65"},
{"kon":"Kvinna","alder":"55-59","pulsbredd":"1","duration":"3","stromstyrka":"800","frekvens":"70","laddning":"336","energi":"65"},
{"kon":"Kvinna","alder":"60-64","pulsbredd":"1","duration":"3,5","stromstyrka":"800","frekvens":"70","laddning":"392","energi":"75"},
{"kon":"Kvinna","alder":"65-69","pulsbredd":"1","duration":"3","stromstyrka":"800","frekvens":"70","laddning":"336","energi":"70"},
{"kon":"Kvinna","alder":"70-74","pulsbredd":"1","duration":"3,5","stromstyrka":"800","frekvens":"70","laddning":"392","energi":"75"},
{"kon":"Kvinna","alder":"75-79","pulsbredd":"1","duration":"5","stromstyrka":"800","frekvens":"50","laddning":"400","energi":"80"},
{"kon":"Kvinna","alder":"80-84","pulsbredd":"1","duration":"5","stromstyrka":"800","frekvens":"50","laddning":"400","energi":"80"},
{"kon":"Man","alder":"18-24","pulsbredd":"1","duration":"3","stromstyrka":"800","frekvens":"50","laddning":"240","energi":"50"},
{"kon":"Man","alder":"25-29","pulsbredd":"1","duration":"3","stromstyrka":"800","frekvens":"50","laddning":"240","energi":"50"},
{"kon":"Man","alder":"30-34","pulsbredd":"1","duration":"3","stromstyrka":"800","frekvens":"60","laddning":"288","energi":"55"},
{"kon":"Man","alder":"35-39","pulsbredd":"1","duration":"2,5","stromstyrka":"800","frekvens":"70","laddning":"280","energi":"55"},
{"kon":"Man","alder":"40-44","pulsbredd":"1","duration":"3","stromstyrka":"800","frekvens":"70","laddning":"336","energi":"60"},
{"kon":"Man","alder":"45-49","pulsbredd":"1","duration":"3","stromstyrka":"800","frekvens":"70","laddning":"336","energi":"65"},
{"kon":"Man","alder":"50-54","pulsbredd":"1","duration":"3","stromstyrka":"800","frekvens":"70","laddning":"336","energi":"70"},
{"kon":"Man","alder":"55-59","pulsbredd":"1","duration":"3","stromstyrka":"800","frekvens":"70","laddning":"336","energi":"70"},
{"kon":"Man","alder":"60-64","pulsbredd":"1","duration":"4","stromstyrka":"800","frekvens":"60","laddning":"384","energi":"80"},
{"kon":"Man","alder":"65-69","pulsbredd":"1","duration":"5","stromstyrka":"800","frekvens":"50","laddning":"400","energi":"80"},
{"kon":"Man","alder":"70-74","pulsbredd":"1","duration":"5,5","stromstyrka":"800","frekvens":"50","laddning":"440","energi":"85"},
{"kon":"Man","alder":"75-79","pulsbredd":"1","duration":"5,5","stromstyrka":"800","frekvens":"50","laddning":"440","energi":"90"},
{"kon":"Man","alder":"80-84","pulsbredd":"1","duration":"5,5","stromstyrka":"800","frekvens":"50","laddning":"440","energi":"85"}];



var randDataOld = [{"kon":"Man","alder":"85+","pulsbredd":"1","duration":"5,5","stromstyrka":"800","frekvens":"50","laddning":"440","energi":"90"}, {"kon":"Kvinna","alder":"85+","pulsbredd":"1","duration":"5,5","stromstyrka":"800","frekvens":"50","laddning":"440","energi":"85"}, {"kon":"Man","alder":"85+","pulsbredd":"0,5","duration":"8","stromstyrka":"800","frekvens":"70","laddning":"448","energi":"90"}, {"kon":"Kvinna","alder":"85+","pulsbredd":"0,5","duration":"7,5","stromstyrka":"800","frekvens":"70","laddning":"420","energi":"85"}]; 

function getAge() {
	var dateOfBirth = RCC.Utils.parseYMD(inca.form.env._FODELSEDATUM) || RCC.Utils.parseYMD(inca.form.env._PERSNR.slice(0,8));
	var age;
	if (dateOfBirth)
		age = RCC.Vast.Utils.calculateAge(RCC.Utils.parseYMD(inca.form.env._FODELSEDATUM) || RCC.Utils.parseYMD(inca.form.env._PERSNR.slice(0,8)), new Date());
	else
		age = parseInt(prompt('Ange patientens ålder i hela år.', ''));

	if (!(age >= 0))
		throw new Error('Kunde inte avgöra patientens ålder.');
	return age;
}

(function () {
  
	var markAsFatal = RCC.Vast.Utils.markAsFatal,
		addNumMinMaxValidation = RCC.Vast.Utils.addNumMinMaxValidation,
		addYearValidation = RCC.Vast.Utils.addYearValidation;
	

	eventHandlers.rowAdded = {
		Random: function (r) 
		{
			r.sjukhusModul = new SjukhusModul(r.sjukhus);
			if(inca.errand && !inca.form.isReadOnly) {
				r.randomiseringsdatum(moment(inca.serverDate).format('YYYY-MM-DD'));
				var val = getAge() > 60 ? 'alderOver60' : 'alderUnder60';
				r.aldersgrupp(ko.utils.arrayFirst(r.aldersgrupp.rcc.term.listValues, function (list) {
			        return list.value === val;
			    }));
			}
			markAsFatal(r.aldersgrupp);
			markAsFatal(r.sjukhus);
		}
	};
})();

$(function () {

	var actions = { abort: ['Avbryt', 'Radera'], pause: ['Pausa'] };

	try {
		var vm = new RCC.ViewModel({
			validation: new RCC.Validation(),
			events: eventHandlers
		});

		// För offline.
		vm.showRegvarNames = ko.observable(window.showRegvarNames);

		var SendForm = function () {
			var errors = vm.$validation.errors(),
			selectedAction = (function() {
				if (inca.errand && inca.errand.action && inca.errand.action.find)
					return inca.errand.action.find(':selected').text();
			})();

			if ($.inArray(selectedAction, actions.abort) >= 0)
				return true;

			if (errors.length > 0) {
				vm.$validation.markAllAsAccessed();

				var isPause = ($.inArray(selectedAction, actions.pause) >= 0);
				var fatalErrors = $.grep(errors,
					function (error) {
						if (isPause && error.data && error.data.canBeSaved )
							return false;
						else
							return error.fatal;
					});

				if (fatalErrors.length > 0) {
					alert("Formuläret innehåller " + fatalErrors.length + " fel.");
					return false;
				} else if (isPause && fatalErrors.length == 0) return true;

			   //return confirm('Valideringen av frågosvaren gav ' + errors.length + ' varning' + (errors.length == 1 ? '' : 'ar') + '.\r\nVill du gå vidare ändå?');
			}

			return false;
		};


		vm.randDataArr = ko.observableArray([]);
		vm.obsPulsbredd = ko.observable('');
		vm.showRandTable = ko.observable(false);
		vm.isPatientRandomized = ko.observable(false);


	    vm.findRandInfo = function(pulsBredd) {
			var kon = inca.form.env._SEX == 'M' ? 'Man' : 'Kvinna';
			var alder = parseInt(getAge());
			if(alder && kon) {
				if(alder > 84) {
					vm.randDataArr.push(_.find(randDataOld, function(row) {
						return pulsBredd == row.pulsbredd && kon == row.kon;
					}));
				}
				else {
					vm.randDataArr.push(_.find(randData, function(row) {
						var startAge = parseInt(row.alder.substr(0,2));
						var endAge = parseInt(row.alder.substr(3,2));
						return pulsBredd == row.pulsbredd && kon == row.kon && alder >= startAge && alder <= endAge;
					}));
				}
			}
		};

		if(!inca.errand) {
			vm.showRandTable(true);
			vm.findRandInfo(vm.pulsbreddgrupp().value);
		} 
		else {
			inca.form.getValueDomainValues({
				vdlist: 'VD_pulsbredd',
				parameters: { patId: inca.form.env._PATIENTID },
				success: function (list) { if(list[0]) vm.isPatientRandomized(true); }, 
				error: function (err) {console.log(err); }
			});
		}

		vm.getErrand = function() {
			if(vm.isPatientRandomized()) {
				alert('Patienten är redan randomiserad, välj Avbryt!');
				return;
			}
			if(!vm.sjukhus() || !vm.aldersgrupp()) {
				alert('Sjukhus och åldersgrupp måste fyllas i först!');
				return;
			};
			if(getAge() < 18) {
				alert('Patienter under 18 år ska inte ingå i studien');
				return;
			};
			
			$.ajax({
	            url: "https://psykiatri.incanet.se/api/Errands/" + inca.form.data.errandId,
	            type: "GET",
	            contentType: "application/json; charset=utf-8",
	            success: function(data) { vm.executeErrand(data); }, 
	            error: function(err) { 
	            	console.log('fail'); 
	            } 
	        });
		};

		vm.executeErrand = function(errand) {
			if(!errand) {
				console.log('Misslyckades hämta ärende info');
				return;
			}
			$.ajax({
	            url: "https://psykiatri.incanet.se/api/Errands/" + inca.form.data.errandId + "/Event/" + 2 + "/Execute",
	            type: "PUT",
	            contentType: "application/json; charset=utf-8",
	            data: JSON.stringify({ data: inca.form.data, version: errand.version }),
	            success: function(res) {
	                inca.form.getValueDomainValues({
						vdlist: 'VD_pulsbredd',
						parameters: { patId: inca.form.env._PATIENTID },
						success: function (list) { 
							if(list[0] && list[0].data && list[0].data.pulsbreddgrupp_Värde) {
								vm.findRandInfo(list[0].data.pulsbreddgrupp_Värde);
								vm.obsPulsbredd(list[0].data.pulsbreddgrupp_Värde);
								vm.showRandTable(true);
							}
						}, 
						error: function (err) { 
							console.log(err); 
						} 
					});
	            },
	            error: function(err) { 
	            	console.log('fail'); 
	            } 
	        });
		};

		vm.pendingRequests = ko.observable(0);

		inca.on('validation', SendForm);
		// XXX: debug
		window.SendForm = SendForm;

		vm.errors = ko.observableArray([]);
		var ready = ko.observable(false);
		vm.showForm = ko.computed( function ( ) { return ready() && (vm.errors().length == 0 || vm.$form.isReadOnly) && vm.pendingRequests() == 0; } );
		vm.enableHelpText = ko.observable(true);
		ko.applyBindings(vm);
		ready(true);

		// XXX: debug
		window.vm = vm;
	} catch (ex) {
		inca.on("validation", function () {
			if (inca.errand && inca.errand.action && $.inArray(inca.errand.action.find ? inca.errand.action.find(':selected').text() : undefined, actions.abort) != -1) return false;
		});
		alert("Ett fel inträffade\r\n\r\n" + ex + "\r\n" + JSON.stringify(ex));  
	}

});



