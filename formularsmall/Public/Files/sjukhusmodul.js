function SjukhusModul(observable)
{
	var self = this;
	function sortAZ(arr)
	{
	    arr.sort(function (a, b) {
	        if (a.name < b.name) return -1;
	        if (a.name > b.name) return 1;
	        return 0;
	    })
	}

	self.regioner = (function() {
		var rv = [
		    {
		        name: 'Norra',
		        landsting: [
		            { name: 'Region Västerbotten', sjukhus: ['umea', 'skelleftea'] },
		            { name: 'Region Västernorrland', sjukhus: ['ornskoldsvik', 'sundsvall', 'solleftea'] },
		            { name: 'Region Norrbotten', sjukhus: ['gallivare', 'pitea', 'sunderbyn'] },
		            { name: 'Region Jämtland Härjedalen', sjukhus: ['ostersund'] }
		        ]
		    },
		    {
		        name: 'Uppsala-Örebro',
		        landsting: [
		            { name: 'Region Örebro län', sjukhus: ['orebro', 'karlskoga', 'lindesberg'] },
		            { name: 'Region Uppsala län', sjukhus: ['uppsala'] },
		            { name: 'Region Dalarna', sjukhus: ['sater', 'falun', 'mora'] },
		            { name: 'Region Sörmland', sjukhus: ['nykoping', 'eskilstuna', 'katrineholm_kullbergska'] },
		            { name: 'Region Värmland', sjukhus: ['karlstad', 'arvika'] },
		            { name: 'Region Gävleborg', sjukhus: ['hudiksvall', 'gavle'] },
		            { name: 'Region Västmanland', sjukhus: ['vasteras'] }
		        ]
		    },
		    {
		        name: 'Stockholm',
		        landsting: [
		            {
		                name: 'Region Stockholm',
		                sjukhus: ['lowenstromska', 'danderyd', 'sodertalje', 'st_goran', 'huddinge', 'karolinska_solna', 'sodersjukhuset']
		            },
		            { name: 'Region Gotland', sjukhus: ['visby'] }
		        ]
		    },
		    {
		        name: 'Sydöstra',
		        landsting: [
		            { name: 'Region Östergötland', sjukhus: ['linkoping', 'norrkoping', 'motala'] },
		            { name: 'Region Kalmar län', sjukhus: ['kalmar', 'vastervik'] },
		            { name: 'Region Jönköpings län', sjukhus: ['varnamo', 'eksjo', 'jonkoping_ryhov'] }
		        ]
		    },
		    {
		        name: 'Västra',
		        landsting: [
		            { name: 'Västra Götalandsregionen', sjukhus: ['boras', 'kungalv', 'su_sahlgrenska', 'su_ostra_sjukhuset', 'falkoping', 'su_molndal', 'trollhattan_nal', 'uddevalla'] },
		            { name: 'Region Halland, norra', sjukhus: ['varberg'] }

		        ]
		    },
		    {
		        name: 'Södra',
		        landsting: [
		            { name: 'Region Skåne', sjukhus: ['trelleborg', 'lund', 'malmo', 'kristianstad', 'hassleholm', 'helsingborg', 'angelholm', 'landskrona'] },
		            { name: 'Region Kronoberg', sjukhus: ['vaxjo', 'ljungby'] },
		            { name: 'Region Halland, södra', sjukhus: ['halmstad'] },
		            { name: 'Region Blekinge', sjukhus: ['karlskrona', 'karlshamn'] }
		        ]
		    }
		];

		sortAZ(rv);
		return rv;

	})();
	
	self.visible = ko.observable(false);
	self.selectedRegion = ko.observable();
	self.selectedLandsting = ko.observable();
	self.selectedSjukhus = ko.observable();

	var allaSjukhus = observable.rcc.term.listValues;
	var allaLandsting = (function()
	{
		var rv = [];
		$.each(self.regioner, function (i, o) {
		    $.each(o.landsting, function (ii, oo) {
		        rv.push(oo);
		    });
		});

		sortAZ(rv);
		return rv;
	})();

	self.landsting = ko.computed(function ()
	{
	    var reg = self.selectedRegion();
	    if (reg)
	    {
	        return reg.landsting;
	    }
	    else
	    {
	        return allaLandsting;
	    }
	});

	self.sjukhus = ko.computed(function ()
	{
	    var ls = self.selectedLandsting();
	    if (ls)
	    {
	        return $.grep(allaSjukhus, function (x)
	        {
	            return $.inArray(x.value, ls.sjukhus) != -1;
	        });
	    }
	    else if (self.selectedRegion()) 
	    {
	        var sjukhusIRegion = [];
	        $.each(self.selectedRegion().landsting, function (i, o)
	        {
	            $.each(o.sjukhus, function (ii, oo)
	            {
	                sjukhusIRegion.push(oo);
	            }); 
	        });
	        return $.grep(allaSjukhus, function (x)
	        {
	            return $.inArray(x.value, sjukhusIRegion) != -1;
	        });
	    }
	    else
	    {
	        return allaSjukhus;
	    }
	});

	self.save = function()
	{
		var v = self.selectedSjukhus();
		observable(ko.utils.arrayFirst(allaSjukhus, function (x)
		{
			return x.value == v;
		}));
		self.visible(false);
	};

	observable.subscribe(function ()
	{
	    var v = observable();
	    self.selectedRegion(undefined);
	    self.selectedLandsting(undefined);
	    self.selectedSjukhus(v ? v.value : undefined);
	});
}