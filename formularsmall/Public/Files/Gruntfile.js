/*global module:false*/
module.exports = function(grunt) {
	
	var sources = (function() {
		var r = { all: [], prod: [] };
		[
			{ src: 'offline/dummy-inca-object-part1.js', offlineOnly: true },
			{ src: 'maestro2/RCC.js' },
			{ src: 'maestro2/RCC/Vast/Utils.js' },
			{ src: 'maestro2/rcc-decimal.js' },
			{ src: 'maestro2/datepicker.js' },
			{ src: 'maestro2/single-answer-component.js' },
			{ src: 'offline/dummy-inca-object-part2.js', offlineOnly: true },
			{ src: 'maestro2/RCC/Vast/Utils/Offline.js', offlineOnly: true },
			{ src: 'offline/offline.js', offlineOnly: true },
			{ src: 'offline/debug.js', offlineOnly: true },
			{ src: 'sjukhusmodul.js' },
			{ src: 'formular.js' }
		].forEach(function(o) {
			r.all.push(o.src);
			if (!o.offlineOnly) r.prod.push(o.src);
		});	
		return r;
	})();

	var src = 
	// Project configuration.
	grunt.initConfig({
		// Metadata.
		pkg: grunt.file.readJSON('package.json'),
		banner: '/*! <%= pkg.title || pkg.name %> | <%= grunt.template.today("yyyy-mm-dd HH:MM:ss") %> */\n',
		// Task configuration.
		concat: {
			options: {
				banner: '<%= banner %>',
				stripBanners: true
			},
			prod: {
				src: sources.prod,
				dest: 'formular/<%= pkg.name %>/<%= pkg.name %>.js'
			},
			offline: {
				src: sources.all,
				dest: 'formular/<%= pkg.name %>/<%= pkg.name %>.offline.js'
			}
		},
		uglify: {
			options: {
				//banner: '<%= banner %>',
				preserveComments: 'some'
				
			},
			prod: {
				src: '<%= concat.prod.dest %>',
				dest: 'formular/<%= pkg.name %>/<%= pkg.name %>.min.js'
			},
			offline: {
				src: '<%= concat.offline.dest %>',
				dest: 'formular/<%= pkg.name %>/<%= pkg.name %>.offline.min.js'
			}
		},
		less: {
			options: {
				compress: true
			},
			build: {
				files: {
				  'formular/<%= pkg.name %>/<%= pkg.name %>.min.css': 'style.less'
				}
			}
		}
	});

	// These plugins provide necessary tasks.
	grunt.loadNpmTasks('grunt-contrib-concat');
	grunt.loadNpmTasks('grunt-contrib-uglify');
	grunt.loadNpmTasks('grunt-contrib-less');

	// Default task.
	grunt.registerTask('default', ['concat', 'uglify', 'less']);

};
