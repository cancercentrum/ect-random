/*! ect-random | 2024-01-04 07:36:11 */
window.inca =
{
	user: { role: { isReviewer: false }, region: { id: -1, name: 'Väst' }, position: { id: -1 } },
	on: function ( ) { if ( window.console ) console.warn( 'inca.on not implemented' ); },
	form:
	{
		isReadOnly: false,
		getRegisterRecordData:
			function ( )
			{
				throw new Error('unimplemented');
			},
		getValueDomainValues:
			function ( opt )
			{
				if ( opt.success )
				{
					setTimeout(
						function ( )
						{
							switch ( opt.vdlist )
							{
								/*
								case 'VD_NjureProcess_Uppf':
									opt.success( [{"text":"x","id":38,"data":{"R1490T17362_ID":38,"R92T322_18112_RappDat":"2012-01-01","R1490T17362_ID2":38,"uppdaterad":"2012-10-30"}},{"text":"x","id":58,"data":{"R1490T17362_ID":58,"R92T322_18112_RappDat":"2012-01-01","R1490T17362_ID2":58,"uppdaterad":"2012-11-09"}}] );
									break;
								*/
								case 'VD_Registernamn_Nyreg':
									opt.success( [{"text":"x","id":4,"data":{"inregistreringsdatum":"2014-01-05"}},{"text":"x","id":49,"data":{"inregistreringsdatum":"2013-11-05"}}] );
									break;
							}
						}, (1+Math.random())*250 ); 
				}
			}
	},
	errand: { status: { val: function () { return 'Nytt ärende'; } } }
};

(function(f){f.RCC={metadataProperty:"rcc",bindingsPrefix:"rcc-"}})(window);(function(f,d,b){var a=b.bindingsPrefix+"included";b.BindingHandlers={};b.BindingHandlers.nearestVariable=function(b,a){for(var c=[].concat(a.$data,a.$parents,a.$root),d=0;d<c.length;d++)if(c[d].hasOwnProperty(b))return c[d][b]};var c=b.bindingsPrefix+"var";d.bindingHandlers[c]={init:function(c,h,g,p,l){var j=h();if(!j[b.metadataProperty])throw Error('The binding "'+b.bindingsPrefix+'var" requires an observable from RCC.ViewModel as argument.');"checkbox"==c.type||"radio"==c.type?d.utils.registerEventHandler(c,
"change",function(){j[b.metadataProperty].accessed(!0)}):(d.utils.registerEventHandler(c,"blur",function(){j[b.metadataProperty].accessed(!0)}),d.utils.registerEventHandler(c,"focusout",function(){j[b.metadataProperty].accessed(!0)}));var k={},n=f(c),q=function(b,a){if(!f.fn.qtip)return{destroy:function(){}};var c=b.data("qtip");b.removeData("qtip");var d=b.qtip(f.extend({},a)).qtip("api");b.data("qtip",c);return d},m={};d.utils.arrayForEach(["$form","$user","$opt"],function(a){m[a]=b.BindingHandlers.nearestVariable(a,
l)});if(!m.$form||!m.$user)throw Error("Unable to find $form or $user in binding context.");m.$opt||(m.$opt={});m.$form.isReadOnly?n.prop(n.is('select, option, button, input[type="checkbox"], input[type="radio"]')?"disabled":"readOnly",!0):m.$user.role.isReviewer&&(k.monitor=q(n,{position:{my:"middle left",at:"middle right"},content:{text:'<input type="checkbox" class="rcc-monitor-include" title="Inkludera">'},suppress:!1,events:{render:function(a,c){d.applyBindingsToNode(c.elements.tooltip.find(".rcc-monitor-include")[0],
{checked:j[b.metadataProperty].include},l)}},hide:{event:!1,inactive:3E3}}),d.computed(function(){d.utils.toggleDomNodeCssClass(c,a,j[b.metadataProperty].include())}));d.computed(function(){if(d.utils.unwrapObservable(m.$opt.displayNames)&&n.is(":visible")){if(!k.name)k.name=q(n,{position:{my:"middle left",at:"middle right"},content:{text:f("<div>").text(j[b.metadataProperty].regvarName).html()},suppress:false,show:{ready:true},hide:{event:false}})}else if(k.name){k.name.destroy();delete k.name}});
"tooltip"in j[b.metadataProperty]&&(j[b.metadataProperty].tooltip.hasOwnProperty("html")?k.tooltip=q(n,{position:{my:"top center",at:"bottom center"},content:{text:'<div class="rcc-tooltip"></div>'},events:{render:function(a,c){d.applyBindingsToNode(c.elements.tooltip.find(".rcc-tooltip")[0],{html:j[b.metadataProperty].tooltip.html},l)}},show:{delay:1E3},hide:{event:"mouseleave"}}):j[b.metadataProperty].tooltip.hasOwnProperty("text")&&n.attr("title",j[b.metadataProperty].tooltip.text));d.utils.domNodeDisposal.addDisposeCallback(c,
function(){for(var a in k)k.hasOwnProperty(a)&&k[a].destroy()})}};d.utils.arrayForEach(["text","value","checked","textInput"],function(a){d.bindingHandlers[b.bindingsPrefix+a]={init:function(h,g,f,l,j){l=g();if(!d.isObservable(l)||!l[b.metadataProperty])throw Error(b.bindingsPrefix+a+" requires an observable from RCC.ViewModel as argument.");var k={};d.utils.arrayForEach([a,c],function(c){if(c in f())throw Error("Cannot bind "+b.bindingsPrefix+a+" and "+c+" to the same element.");k[c]=g()});d.applyBindingsToNode(h,
k,j)}}});d.bindingHandlers[b.bindingsPrefix+"list"]={init:function(a,h,g,f,l){var j=h(),h=j[b.metadataProperty];if(!h||!h.term||!h.term.listValues)throw Error('The binding "'+b.bindingsPrefix+'list" requires a list observable from RCC.ViewModel as argument.');g=g();h=h.term.listValues.filter(function(a){var b=new Date(a.validFrom),c=new Date,d=new Date(a.validTo);return j()&&j().id===a.id||!a.validFrom||!a.validTo||b<=c&&d>=c});j[b.metadataProperty].validation&&j[b.metadataProperty].validation.add(d.computed(function(){var a=
j();if(a&&a.validFrom&&a.validTo){var b=new Date(a.validFrom),c=new Date,a=new Date(a.validTo),d=function(a){var b=function(a){return 10>a?"0"+a:a};return a.getFullYear()+"-"+b(a.getMonth())+"-"+b(a.getDate())};if(b>c)return{result:"failed",fatal:!1,message:"Listv\u00e4rdet b\u00f6rjar g\u00e4lla f\u00f6rst "+d(b)};if(a<c)return{result:"failed",fatal:!1,message:"Listv\u00e4rdet utgick "+d(a)}}}));h={value:j,options:h,optionsText:"text",optionsCaption:"\u2013 V\u00e4lj \u2013"};h[c]=j;for(var k in h)h.hasOwnProperty(k)&&
g.hasOwnProperty(k)&&(h[k]=g[k]);d.applyBindingsToNode(a,h,l)}}})(window.jQuery,window.ko,window.RCC);(function(f,d,b){var a={};b.ViewModel=function(b){b||(b={});b.incaForm||(b.incaForm=f.form);if(!("rootTable"in b)){var e=[],h={},g;for(g in b.incaForm.metadata)if(b.incaForm.metadata.hasOwnProperty(g)){e.push(g);for(var p=b.incaForm.metadata[g].subTables,l=0;l<p.length;l++)h[p[l]]=!0}e=d.utils.arrayFilter(e,function(b){return!h[b]});if(1==e.length)b.rootTable=e[0];else throw Error('Must supply "rootTable" option for RCC.ViewModel ('+(0==e.length?"no candidates found":"multiple candidates found: "+
e.join(", "))+").");}b.incaUser||(b.incaUser=f.user);b.events||(b.events={});b.registerVariableDocumentation||(b.registerVariableDocumentation=d.observableArray(),b.registerId&&(e=f.getBaseUrl()+"api/Registers/"+b.registerId+"/RegisterVariables/Documentation",$.get(e).done(function(a){typeof a=="string"&&(a=JSON.parse(a));b.registerVariableDocumentation(a)})));e=a.createViewModel(b.rootTable,b.incaForm.data,b.incaForm.metadata,b.incaForm.createDataRow.bind(b.incaForm),[],b);e.$events=b.events;e.$env=
b.incaForm.env;e.$form=b.incaForm;e.$user=b.incaUser;e.$opt=b.opt||{};b.validation&&(e.$validation=b.validation,b.validation.track(e));return e};b.ViewModel.fill=function(b){return a.fillViewModel(b.target,b.data)};b.ViewModel.prototype.fill=function(b){return a.fillViewModel(this,b)};(function(a){var e,h,g,f,l,j,k,n,q,m,r,v,s,t;e="$$";h="$vd";g=b.metadataProperty;f="add";l="remove";j="include";k="accessed";n="regvar";q="regvarName";m="term";r="tooltip";v="documentation";s="dependencies";t="dependenciesFulfilled";
var u=function(){function b(a,c,e,h,i){var f=c[h].value,i=i||h;"list"===e.dataType?f=d.utils.arrayFirst(e.listValues,function(a){return a.id===f})||void 0:"decimal"===e.dataType&&"number"===typeof f&&(f=f.toFixed(e.precision).replace(/\./,","));a[i]=d.observable(f);a[i].subscribe(function(a){c[h].value=a&&"list"===e.dataType?a.id:a});if(g in a[i])throw Error('The created ko.observable already has a property with name "'+g+'"');a[i][g]={};a[i][g][j]=d.observable(c[h].include);a[i][g][j].subscribe(function(a){c[h].include=
a});a[i][g][k]=d.observable(!1);a[i][g][s]=d.observableArray();a[i][g][t]=d.computed(function(){var b=a[i][g][s]();return b&&b.reduce(function(a,b){return a&&b()},!0)});a[i][g][t].subscribe(function(b){b||(a[i](null),a[i][g][k](!1))})}function a(c,e,h,f){var i,j;for(i in h.regvars)h.regvars.hasOwnProperty(i)&&(j=h.terms[h.regvars[i].term],b(c,e.regvars,j,i,void 0),c[i][g][n]=h.regvars[i],c[i][g][m]=j,c[i][g][q]=i,function(){var a=i;c[a][g][v]=d.computed(function(){return _.find(f.registerVariableDocumentation(),
function(b){return b.shortName==a})})}(),h.regvars[i].description&&f&&f.opt&&("html"==f.opt.tooltips?c[i][g][r]={html:d.observable(h.regvars[i].description)}:"text"==f.opt.tooltips&&(c[i][g][r]={text:h.regvars[i].description})),f&&f.validation&&f.validation.init(c[i],{source:{viewModel:c[i],name:i,type:"regvar"}}))}function c(a,d,e,f){var i,j;a[h]={};for(i in e.vdlists)e.vdlists.hasOwnProperty(i)&&(j=e.terms[e.vdlists[i].term],b(a[h],d.vdlists,j,i,i),a[h][i][g][m]=e.terms[e.vdlists[i].term],f&&f.validation&&
f.validation.init(a[h][i],{source:{name:i,type:"vdlist"}}))}function x(a,b,c,h,i,j,k){var o,m,n,q;for(m=0;m<c.subTables.length;m+=1){o=c.subTables[m];a[e][o]=d.observableArray();a[e][o][g]={};a[e][o][g][f]=w.createAdd(a,o,h,i,j,k);a[e][o][g][l]=w.createRemove(a,o);for(n=0;n<b.subTables[o].length;n+=1)q=u(o,b.subTables[o][n],h,i,j,k),a[e][o].push(q)}}var w={createAdd:function(a,b,d,c,h,f){return function(g){g=g||u(b,void 0,d,c,h,f);a.$__data.subTables[b].push(g.$__data);a[e][b].push(g);return g}},
createRemove:function(a,b){return function(c){c.$__data.id?c.$__data._destroy=!0:d.utils.arrayRemoveItem(a.$__data.subTables[b],c.$__data);a[e][b].remove(c)}}};return function(b,h,f,g,i,j){var k=f[b],l={},p=!1;if(void 0===k)throw Error('Metadata for table "'+b+'" not found');void 0===h&&(h=g(b),p=!0);l.$__data=h;l[e]={};a(l,h,k,j);c(l,h,k,j);x(l,h,k,f,g,i.concat(l),j);p&&j&&j.events&&"rowCreated"in j.events&&d.utils.arrayForEach([].concat(j.events.rowCreated[b]||[]),function(a){a.call(l,l,i.slice(0))});
j&&j.events&&"rowAdded"in j.events&&d.utils.arrayForEach([].concat(j.events.rowAdded[b]||[]),function(a){a.call(l,l,i.slice(0))});return l}}();a.createViewModel=u;a.fillViewModel=function(a,b){var c,e;for(c in b)if(b.hasOwnProperty(c)&&d.isWriteableObservable(a[c]))if(e=a[c],"list"===e[g].term.dataType)a[c](d.utils.arrayFirst(e[g].term.listValues,function(a){return a.id==b[c]})||void 0);else if("decimal"===e[g].term.dataType&&"number"===typeof b[c])a[c](b[c].toFixed(e[g].term.precision).replace(/\./,
","));else a[c](b[c])}})(a)})(window.inca,window.ko,window.RCC,window);(function(f,d){d.Utils={};d.Utils.createDummyForm=function(b){function a(b,c){h[b]={regvars:{},subTables:{},vdlists:{}};d[b]={regvars:{},vdlists:{},subTables:[],terms:{}};f.each(["regvars","vdlists"],function(a,k){f.each(c[k]||{},function(a,c){g++;h[b][k][a]={include:!0,value:null};d[b][k][a]={term:g};d[b].terms[g]=f.isArray(c)?{description:"term for "+a,dataType:"list",listValues:f.map(c,function(a){return"object"==typeof a?a:{id:a,value:a,text:a}})}:c.term?c.term:{description:"term for "+a,dataType:c}})});
f.each(c.subTables||{},function(c,f){d[b].subTables.push(c);h[b].subTables[c]=[];a(c,f)})}var c={env:{_PERSNR:"19111111-1111",_FIRSTNAME:"Test",_SURNAME:"Testsson",_INREPNAME:"Rapport\u00f6r Rapport\u00f6rsson",_INUNITNAME:"OC Demo (0)",_SEX:"M",_ADDRESS:"Testgatan 1",_POSTALCODE:"12345",_CITY:"Teststad",_LKF:"123456",_SECRET:!1,_DATEOFDEATH:"",_REPORTERNAME:"",_LAN:"12",_KOMMUN:"34",_FORSAMLING:"56",_PATIENTID:1}},d={},h={},g=Math.round(1E3*Math.random());a(b.rootTable,{regvars:b.regvars,subTables:b.subTables});
c.metadata=d;c.createDataRow=function(a){return f.extend(!0,{},h[a])};c.data=c.createDataRow(b.rootTable);c.getContainer=function(){return document.getElementsByTagName("body")[0]};return c};d.Utils.parseYMD=function(b){return(b=(b||"").match(/^(\d{4})(-?)(\d\d)\2(\d\d)$/))?new Date(b[1],parseInt(b[3].replace(/^0/,""),10)-1,b[4].replace(/^0/,"")):void 0};d.Utils.ISO8601=function(b){function a(a){return("0"+a).slice(-2)}return[b.getFullYear(),a(b.getMonth()+1),a(b.getDate())].join("-")}})(window.jQuery,
window.RCC,window);/*

 Portions of this software may utilize the following copyrighted material, 
 the use of which is hereby acknowledged.

 Knockout Validate v0.1-pre
 (c) Martin Land?lv - http://mlandalv.github.com/knockout-validate/)
 MIT license
*/
(function(f,d){d.Validation=function(b){b||(b={});this.viewModels=f.observableArray([]);this.errors=f.computed(d.Validation.prototype._getErrors.bind(this));this.requiredDefault="requiredDefault"in b?b.requiredDefault:!0;this.includedDefault="includedDefault"in b?b.includedDefault:!0;"addDefaultValidations"in b||(b.addDefaultValidations=!0);this.addDefaultValidations=b.addDefaultValidations;this._dummyModel={dummy:{}};this._dummyModel.dummy[d.metadataProperty]={term:{dataType:"rcc-dummy"}};this._dummyModel.dummy[d.metadataProperty].validation=
this.init(this._dummyModel.dummy,{source:{type:"unbound-errors"}});this.track(this._dummyModel)};d.Validation.prototype.add=function(){this._dummyModel.dummy[d.metadataProperty].validation.add.apply(this,Array.prototype.slice.call(arguments,0))};d.Validation.prototype.track=function(b){this.viewModels.push(b)};d.Validation.prototype.init=function(b,a){var c=this,e={};e.errors=f.observableArray([]);e.tests=f.observableArray([]);e.info=a;e.add=function(b){b=new d.Validation.Test({errorsTo:e.errors,
test:b,info:a});e.tests.push(b);return b};var h=b[d.metadataProperty];if(h&&(h.validation=e,c.addDefaultValidations)){var g=h.term;"datetime"==g.dataType&&!g.includeTime?e.add(new d.Validation.Tests.Date(b,{ignoreMissingValues:!0,fatal:!0})):"integer"==g.dataType?e.add(new d.Validation.Tests.Integer(b,{min:g.min,max:g.max,ignoreMissingValues:!0,fatal:!0})):"decimal"==g.dataType&&e.add(new d.Validation.Tests.Decimal(b,{decimalPlaces:g.precision,min:g.min,max:g.max,ignoreMissingValues:!0,fatal:!0}));
void 0!==g.maxLength&&null!==g.maxLength&&e.add(new d.Validation.Tests.Length(b,{max:g.maxLength,treatMissingLengthAsZero:!0,ignoreMissingValues:!0,fatal:!0}));void 0!==g.validCharacters&&null!==g.validCharacters&&e.add(new d.Validation.Tests.ValidCharacters(b,{characters:g.validCharacters,ignoreMissingValues:!0,fatal:!0}));f.utils.arrayForEach(["required","included"],function(a){var b=f.observable(void 0),d=f.observable(!1);e[a]=f.computed({read:function(){return d()?b():f.utils.unwrapObservable(c[a+
"Default"])},write:function(a){b(a);d(!0)}})});0>f.utils.arrayIndexOf(["boolean","rcc-dummy"],h.term.dataType)&&e.add(new d.Validation.Tests.NotMissing(b))}return e};d.Validation.prototype._forEachVariable=function(b,a){f.utils.arrayForEach(b,function e(b){for(var d in b)"$"!=d.substring(0,1)&&b.hasOwnProperty(d)&&b[d]&&a(b[d]);if(b.$vd)for(var p in b.$vd)b.$vd.hasOwnProperty(p)&&b.$vd[p]&&a(b.$vd[p]);if(b.$$)for(var l in b.$$)b.$$.hasOwnProperty(l)&&b.$$[l]&&f.utils.arrayForEach(b.$$[l](),e)})};
d.Validation.prototype._getErrors=function(){var b=[];this._forEachVariable(this.viewModels(),function(a){(a=a[d.metadataProperty])&&a.validation&&a.validation.errors&&f.utils.unwrapObservable(a.validation.included)&&b.push(a.validation.errors())});return f.utils.arrayGetDistinctValues(Array.prototype.concat.apply([],b))};d.Validation.prototype.markAllAsAccessed=function(){this._forEachVariable(this.viewModels(),function(b){(b=b[d.metadataProperty])&&b.accessed&&b.accessed(!0)})};d.Validation.prototype.markAllAsAccessedIfDependenciesAreFulfilled=
function(){this._forEachVariable(this.viewModels(),function(b){(b=b[d.metadataProperty])&&b.accessed&&b.dependenciesFulfilled()&&b.accessed(!0)})}})(window.ko,window.RCC,window);/*

 Portions of this software may utilize the following copyrighted material, 
 the use of which is hereby acknowledged.

 Knockout Validate v0.1-pre
 (c) Martin Land?lv - http://mlandalv.github.com/knockout-validate/)
 MIT license
*/
(function(f,d){d.Validation.Test=function(b){var a=this;a._disposed=f.observable(!1);a._error={info:b.info};a._testReferences=[];a._targets=f.utils.arrayMap([].concat(b.errorsTo),function(b){var e=b[d.metadataProperty];return e&&e.validation?(a._testReferences.push(e.validation.tests),e.validation.tests.push(a),e.validation.errors):b});a._test=f.isSubscribable(b.test)?b.test:f.computed({read:b.test,disposeWhen:a._disposed});f.computed({read:a._update.bind(a),disposeWhen:a._disposed})};d.Validation.Test.prototype.dispose=
function(){var b=this;b._disposed(!0);f.utils.arrayForEach(b._testReferences,function(a){a.remove(b)});b._removeErrorFromTargets()};d.Validation.Test.prototype._removeErrorFromTargets=function(){var b=this;f.utils.arrayForEach(b._targets,function(a){a.remove(b._error)});delete b._error.message};d.Validation.Test.prototype._update=function(){var b=this;if(b._disposed())b._removeErrorFromTargets();else{var a=b._test();if("string"==typeof a)a={result:"failed",message:a};else if("undefined"==typeof a)a=
{result:"ok"};else if("object"!=typeof a)throw Error("Validation test returned bad value: "+a);if("failed"==a.result)b._error.message=a.message,b._error.fatal=a.fatal,b._error.data=a.data,f.utils.arrayForEach(b._targets,function(a){a.remove(b._error);a.push(b._error)});else if("ok"==a.result)b._removeErrorFromTargets();else throw Error("Validation test returned bad result: "+a.result);}}})(window.ko,window.RCC,window);/*

 Portions of this software may utilize the following copyrighted material, 
 the use of which is hereby acknowledged.

 Knockout Validate v0.1-pre
 (c) Martin Land?lv - http://mlandalv.github.com/knockout-validate/)
 MIT license
*/
(function(f,d){d.Validation.Tests={};d.Validation.Tests._hasValue=function(b,a){return void 0!==b&&null!==b&&(!a||"string"!=typeof b||0<b.length)};d.Validation.Tests.Date=function(b,a){a||(a={});a.hasOwnProperty("fatal")||(a.fatal=!1);return function(){var c=b();if(a.ignoreMissingValues&&!d.Validation.Tests._hasValue(c,true))return{result:"ok"};var e=d.Utils.parseYMD(c);return!e?{result:"failed",message:"Datumet kunde inte tolkas (\u00e4r inte p\u00e5 formen \u00c5\u00c5\u00c5\u00c5-MM-DD).",fatal:a.fatal}:
d.Utils.ISO8601(e)!=c?{result:"failed",message:"Felaktigt datum (inte p\u00e5 formen \u00c5\u00c5\u00c5\u00c5-MM-DD).",fatal:a.fatal}:{result:"ok"}}};d.Validation.Tests.Integer=function(b,a){a||(a={});a.hasOwnProperty("fatal")||(a.fatal=!1);return function(){var c=b();if(a.ignoreMissingValues&&!d.Validation.Tests._hasValue(c,true))return{result:"ok"};if(!d.Validation.Tests._hasValue(c)||!c.toString().match(/^[-+]?(?:\d|[1-9]\d*)$/))return{result:"failed",message:"Ogiltigt heltal.",fatal:a.fatal};
c=parseInt(c,10);return d.Validation.Tests._hasValue(a.min)&&c<a.min?{result:"failed",message:"Minsta till\u00e5tna v\u00e4rde: "+a.min,fatal:a.fatal}:d.Validation.Tests._hasValue(a.max)&&c>a.max?{result:"failed",message:"St\u00f6rsta till\u00e5tna v\u00e4rde: "+a.max,fatal:a.fatal}:{result:"ok"}}};d.Validation.Tests.Decimal=function(b,a){a.hasOwnProperty("fatal")||(a.fatal=!1);return function(){var c=b();if(a.ignoreMissingValues&&!d.Validation.Tests._hasValue(c,true))return{result:"ok"};var e=RegExp("^[-+]?(?:\\d|[1-9]\\d*),\\d{"+
a.decimalPlaces+"}$");if(!d.Validation.Tests._hasValue(c)||!c.toString().match(e))return{result:"failed",message:"Ej decimaltal med "+a.decimalPlaces+" decimal"+(a.decimalPlaces==1?"":"er")+".",fatal:a.fatal};c=parseFloat(c.toString().replace(/,/,"."));return d.Validation.Tests._hasValue(a.min)&&c<a.min?{result:"failed",message:"Minsta till\u00e5tna v\u00e4rde: "+a.min,fatal:a.fatal}:d.Validation.Tests._hasValue(a.max)&&c>a.max?{result:"failed",message:"St\u00f6rsta till\u00e5tna v\u00e4rde: "+a.max,
fatal:a.fatal}:{result:"ok"}}};d.Validation.Tests.Length=function(b,a){a||(a={});a.hasOwnProperty("fatal")||(a.fatal=!1);return function(){var c=b();if(a.ignoreMissingValues&&!d.Validation.Tests._hasValue(c,true))return{result:"ok"};if(!d.Validation.Tests._hasValue(c)||typeof c.length=="undefined")if(a.treatMissingLengthAsZero)c=0;else return{result:"failed",message:"L\u00e4ngd saknas.",fatal:a.fatal};else c=c.length;return d.Validation.Tests._hasValue(a.min)&&c<a.min?{result:"failed",message:"Minsta till\u00e5tna l\u00e4ngd: "+
a.min,fatal:a.fatal}:d.Validation.Tests._hasValue(a.max)&&c>a.max?{result:"failed",message:"St\u00f6rsta till\u00e5tna l\u00e4ngd: "+a.max,fatal:a.fatal}:{result:"ok"}}};d.Validation.Tests.ValidCharacters=function(b,a){a.hasOwnProperty("fatal")||(a.fatal=!1);return function(){var c=b();if(a.ignoreMissingValues&&!d.Validation.Tests._hasValue(c,true))return{result:"ok"};if(typeof c!="string")return{result:"failed",message:"Inte en textstr\u00e4ng.",fatal:a.fatal};for(var e=0;e<c.length;e++)if(a.characters.indexOf(c[e])<
0)return{result:"failed",message:"Otill\u00e5tet tecken: "+c[e],fatal:a.fatal};return{result:"ok"}}};d.Validation.Tests.NotMissing=function(b){return function(){var a=b[d.metadataProperty].validation.required();if(!a)return{result:"ok"};var c=b();if(!d.Validation.Tests._hasValue(c)||"string"==typeof c&&0==c.length){var e={result:"failed",message:"V\u00e4rde saknas"};"object"==typeof a&&null!==a&&f.utils.arrayForEach(["fatal","data"],function(b){a.hasOwnProperty(b)&&(e[b]=a[b])});return e}return{result:"ok"}}}})(window.ko,
window.RCC,window);

(function ( inca, window )
{
	var Utils = {};

	if ( !window.RCC )
		window.RCC = {};
	if ( !window.RCC.Vast )
		window.RCC.Vast = {};
	if ( !window.RCC.Vast.Utils )
		window.RCC.Vast.Utils = Utils;

	/**
	 * Beräknar antalet "fyllda år" för en person, där någon född på skottdagen
	 * anses fylla år på skottdagen vid skottår och annars den 1 mars.
	 *
	 * @param {Date} dateOfBirth Födelsedatum, i lokala tidszonen.
	 * @param {Date} date Datum vid vilken åldern ska beräknas, i lokala tidszonen.
	 * @returns {Integer} Antal fyllda år.
	 */
	Utils.calculateAge = function ( dateOfBirth, date )
	{
		if ( date.getTime() < dateOfBirth.getTime() )
			return undefined;

		function y ( date ) { return date.getFullYear(); };
		function m ( date ) { return date.getMonth(); };
		function d ( date ) { return date.getDate(); };

		// Beräkna först åldern vid årsslut (31 december).
		var age = y(date) - y(dateOfBirth);

		// Dra bort 1 år om aktuellt års födelsedag inte passerats. (Avgör hur skottårspersoner hanteras.)
		if ( m(date) < m(dateOfBirth) || (m(date) == m(dateOfBirth) && d(date) < d(dateOfBirth)) )
			age -= 1;

		return age;
	};

	/**
	 * Raderar alla rader i en tabell.
	 * @param table Tabell vars rader ska raderas.
	 */
	Utils.removeAllRows = function ( table )
	{
		$.each( table().slice(0), function (i, o) { table.rcc.remove(o); } );
	};

	/**
	 * Radera eller lägg till rader i undertabeller baserat på en variabels värde.
	 * Samma undertabell får inte hanteras i flera villkor.
	 *
	 * @param row Raden vars undertabeller ska administreras.
	 * @param currentValue Värdet som villkoren ska jämföras med. En ko.subscribable unwrappas 
	 *				först, och ett objekt reduceras till värdet av egenskapen "value".
	 * @param {Object[]} conditions Villkor av typen { table, value }, där tabellen table töms
	 *				om (i) aktuella värdet är skilt (!==) från value, eller (ii) om value är en array
	 *				skilt (!==) från varje element i arrayn, eller (iii) om value är en funktion och
	 *				denna funktion returnerar sant när den anropas med currentValue som argument.
	 *				I annat fall tillgodoses att tabellen har minst en rad.
	 */
	Utils.subscribeHelper = function ( row, currentValue, conditions )
	{
		// Unwrap observables and objects.
		currentValue = ko.utils.unwrapObservable(currentValue);
		if ( typeof currentValue == 'object' && currentValue !== null )
			currentValue = currentValue.value;

		$.each( conditions,
			function ( unused, condition )
			{
				var table = row.$$[condition.table];
				var shouldDelete;
				if ( $.isFunction(condition.value) )
					shouldDelete = !condition.value(currentValue, row);
				else
					shouldDelete = ($.inArray( currentValue, [].concat(condition.value) ) < 0);

				if ( shouldDelete )
					RCC.Vast.Utils.removeAllRows(table);
				else if ( table().length == 0 )
					table.rcc.add();
			} );
	};
	
	/**
	* Lägg till en rad i given tabell om denna saknar rader.
	*
	* @param table Tabellen vars rader ska administreras (en ko.observableArray med egenskap "rcc").
	* @returns {undefined|Object} Raden som lades till, eller undefined om ingen rad lades till.
	*/
	Utils.addRowIfEmpty = function ( table ) {
		if (table().length == 0) {
			return table.rcc.add();
		}
	};


	/**
	 * Tar fram vilken tabell som är rottabell, utgående från given metadata.
	 *
 	 * @param {Object} [metadata=inca.form.metadata] Metadataobjektet.
	 * @returns {String|undefined} Rottabellens namn, eller undefined om en unik rottabell 
	 * 			inte kan bestämmas.
	 */
	Utils.rootTable = function ( metadata )
	{
		if ( !metadata )
			metadata = inca.form.metadata;

		var tables = [], isSubTable = {};
		for ( var table in metadata )
		{
			if ( metadata.hasOwnProperty(table) )
			{
				tables.push( table );

				var subTables = metadata[table].subTables;
				for ( var i = 0; i < subTables.length; i++ )
					isSubTable[ subTables[i] ] = true;
			}
		}

		var rootTables = ko.utils.arrayFilter( tables, function ( table ) { return !isSubTable[table]; } );

		return rootTables.length == 1 ? rootTables[0] : undefined;
	};

	Utils.SubTableConditionHandler = function(rowAddedEventHandler, conditions) {

		// Sortera in villkor i en tabellstruktur och få ett objekt som visar vilka tabeller som ska få ett nytt rowAdded-event.
		var n_cond = {}
		$.each(inca.form.metadata, function (tableName, tableObj) {
			$.each(tableObj.regvars, function (regvarName) {
				if (conditions[regvarName]) {
					if (!n_cond[tableName]) { n_cond[tableName] = []; }
					n_cond[tableName].push({ name: regvarName, conditions: $.isFunction(conditions[regvarName]) ? conditions[regvarName]() : conditions[regvarName] });
				}
			});
		});

		// Skapa rowAdded-event.
		$.each(n_cond, function (tableName, tableConditions) {
			function subTableConditionHandlerEvent(row) {
				$.each(tableConditions, function (i, condition) {
					row[condition.name].subscribe(function () {
						Utils.subscribeHelper(row, row[condition.name], condition.conditions);
					});
				});
			}

			if (rowAddedEventHandler[tableName]) {
				rowAddedEventHandler[tableName] = [].concat(rowAddedEventHandler[tableName], subTableConditionHandlerEvent);
			} else {
				rowAddedEventHandler[tableName] = subTableConditionHandlerEvent;
			}
		});

		/**
		* Hämtar värdet på ett villkor.
		*
		* @param {String} regvarName Namn på egenskap.
		* @param {String} tableName Namn på tabellen där raderna hanteras av subscribeHelper.
		* @returns {String|String[]} Värdet av villkoret.
		*/

		this.getConditionValue = function (regvarName, tableName) {
			var rval;
			var regvarConditions = $.isFunction(conditions[regvarName]) ? conditions[regvarName]() : conditions[regvarName];
			$.each(regvarConditions, function (i, o) {
				if (o.table == tableName) {
					rval = o.value;
					return false;
				}
			});
			return rval;
		};

	};

	/**
	* Returnerar en array med samtliga value-egenskaper i angiven listvariabel.
	* @param {String} varName Namn på listvariabel.
	* @param {String|String[]} excludeValue Värde/värden som ska exkluderas.
	*/

	Utils.getListValuesArray = function(varName, excludeValue)
	{
		var listValues = (function() {
			var rv = null;
			$.each(inca.form.metadata, function(n, o) {
				if (rv) return false;
				$.each(o.regvars, function(nn, oo) {
					if (varName == nn)
					{
						rv = o.terms[oo.term].listValues;
						return false;
					}
				});
			});
			return rv;
		})();

		if (!listValues) throw new Error('List values for ' + varName + ' not found.');
		
		var rv = [];
		excludeValue = (excludeValue ? [].concat(excludeValue) : []);

		$.each(listValues, function(i, o) {
			if ($.inArray(o.value, excludeValue) == -1)
			{
				rv.push(o.value)
			}
		});

		return rv;
	};

	/**
	 * Markera given observable som "required" och "fatal". Felets dataobjekt innehåller
	 * objekt med egenskap "canBeSaved" satt till true.
	 *
	 * @param {ko.observable} Observable från en RCC.ViewModel, med .rcc.validation-egenskap.
	 */

	Utils.markAsFatal = function(observable) {
		observable.rcc.validation.required({ fatal: true, data: { canBeSaved: true } });
	};

	/**
	 * Lägg till validering som kontrollerar att ett numeriskt värde ligger inom
	 * givna gränser, inklusivt. Valideringen görs endast om det verkligen är ett
	 * giltigt numeriskt värde.
	 * 
	 * @param {ko.observable} observable Observable från en RCC.ViewModel, med .rcc.validation-egenskap.
	 * @param {Number} min Minsta tillåtna värde.
	 * @param {Number} max Största tillåtna värde.
	 * @param {Boolean} [isFatal=false] Om sant markeras felet som "fatal".
	 */

	Utils.addNumMinMaxValidation = function(observable, min, max, isFatal) {
		observable.rcc.validation.add(function () {
			var v = observable();
			if (typeof v == 'string')
				v = parseFloat(v.replace(/,/g, '.'));
			isFatal = !!isFatal;

			// Annan validering fångar felaktiga numeriska värden.
			if (v === undefined || v === null || isNaN(v) || !isFinite(v))
				return;
			else if (v > max)
				return { result: 'failed', message: 'Största tillåtna värde: ' + max.toString().replace('.', ','), fatal: isFatal };
			else if (v < min)
				return { result: 'failed', message: 'Minsta tillåtna värde: ' + min.toString().replace('.', ','), fatal: isFatal };
			else
				return;
		});
	};


	/**
	 * Lägg till validering, markerad som "fatal", som kontrollerar att en 
	 * observable innehåller ett värde som "rimligen" representerar ett årtal.
	 * @param {ko.observable} Observable från en RCC.ViewModel,  med .rcc.validation-egenskap.
	 */

	Utils.addYearValidation = function(observable) {
		observable.rcc.validation.add(function () {
			var v = observable();
			if (v !== null && v !== undefined && v !== '') {
				if (!(v + '').match(/^\d{4}$/)) {
					return { result: 'failed', message: 'Felaktigt årtal', fatal: true };
				}
			}
		});

	};

	/**
	 * Returnera en funktion som kan användas som inca.on('validation')-callback.
	 *
	 * @param {RCC.Validation} validation Valideringsobjektet.
	 * @param {Object} [actions={}] Åtgärder.
	 * @param {String[]} [actions.abort=[]] Åtgärder som inte kräver någon validering.
	 * @param {String[]} [actions.pause=[]] Åtgärder som inte kräver strikt validering.
	 * @returns {Function} Funktion som kan användas som inca.on('validation')-callback.
	 */
	Utils.getFormValidator = function ( validation, actions ) {
		if ( !actions ) actions = {};
		if ( !actions.pause ) actions.pause = [];
		if ( !actions.abort ) actions.abort = [];

		return function ( ) {
			var selectedAction = (function() {
				if (inca.errand && inca.errand.action && inca.errand.action.find)
					return inca.errand.action.find(':selected').text();
			})();

			if ($.inArray(selectedAction, actions.abort) >= 0)
				return true;

			var errors = validation.errors();

			if (errors.length > 0) {
				validation.markAllAsAccessed();

				var isPause = ($.inArray(selectedAction, actions.pause) >= 0);
				var fatalErrors = $.grep(errors,
					function (error) {
						if (isPause && error.data && error.data.canBeSaved )
							return false;
						else
							return error.fatal;
					});

				if (fatalErrors.length > 0) {
					alert("Formuläret innehåller " + fatalErrors.length + " fel.");
					return false;
				} else if (isPause && fatalErrors.length == 0) return true;

			   return confirm('Valideringen av frågosvaren gav ' + errors.length + ' varning' + (errors.length == 1 ? '' : 'ar') + '.\r\nVill du gå vidare ändå?');
			}

			return true;
		};
	};

	/**
	 * Extraherar innehållet av den första kommentaren i funktionens kod.
	 *
	 * @param {Function} func Funktion som innehåller utkommenterad kod.
	 * @returns {String}
	 */
	Utils.extractSource = function(func) {
		var m = func.toString().match(/\/\*(?:\*\s*@(?:preserve|license))?([\s\S]*?)\*\//);
		return m && m[1];
	};
	
	/**
	 * Knockouts metod för att detektera version av Internet Explorer. Från knockout-3.2.0.debug.js
	 *
	 * @returns {Number|undefined}
	 */
    Utils.ieVersion = document && (function() {
        var version = 3, div = document.createElement('div'), iElems = div.getElementsByTagName('i');

        // Keep constructing conditional HTML blocks until we hit one that resolves to an empty fragment
        while (
            div.innerHTML = '<!--[if gt IE ' + (++version) + ']><i></i><![endif]-->',
            iElems[0]
        ) {}
        return version > 4 ? version : undefined;
    }());

})( window['inca'], window );

(function (RCC, ko, _) {
    ko.bindingHandlers[RCC.bindingsPrefix + 'decimal'] = {
        init: function (element, valueAccessor, allBindingsAccessor, viewModel, bindingContext) {
            var val = valueAccessor(),
                opt = _.defaults((ko.isObservable(val) ? { valueObservable: val } : val), { allowPointAsDecimalSeparator: false, padWithZeros: true }),
                metadata = opt.valueObservable[RCC.metadataProperty];

            if (!metadata || !metadata.term || metadata.term.dataType != "decimal")
                throw new Error('The binding "' + RCC.bindingsPrefix + 'decimal" requires a decimal observable from RCC.ViewModel as argument.');

            var precision = opt.valueObservable[RCC.metadataProperty].term.precision;
            var bindings = {
                event: {
                    change: function () {
                        var value = opt.valueObservable();
                        if (value) {
                            if (opt.allowPointAsDecimalSeparator) value = value.replace(/\./g, ',');
                            if (opt.padWithZeros && new RegExp("^-?(?:\\d+|\\d*,\\d{1," + precision + "})$").exec(value))
                                value = parseFloat(value.replace(/,/g, '.')).toFixed(precision).toString().replace(/\./g, ',');
                            opt.valueObservable(value);
                        }
                        return true;
                    }
                }
            };
            var valueBinding = {};
            valueBinding[RCC.bindingsPrefix + 'value'] = opt.valueObservable;
            ko.applyBindingsToNode(element, valueBinding, bindingContext);
            ko.applyBindingsToNode(element, bindings, bindingContext);
        }
    };
})(window['RCC'], window['ko'], window['_']);
(function ($, _, RCC, ko, moment) {
    ko.components.register('date-picker', {
        synchronous: !RCC.Vast.Utils.ieVersion || RCC.Vast.Utils.ieVersion > 8,
        viewModel: function(params) {
            if (!params.value) throw new Error('Parameter "value" is required.');
            _.defaults(params, { autoOpen: false, closable: true, readOnly: false });

            var self = this, today = (params.dateToday ? Moment(params.dateToday) : Moment());
            self.visible = ko.observable(params.autoOpen);
            self.pickerMonth = ko.observable();

            if (params.closable) {
                self.close = function() {
                    self.visible(false);
                };
                self.toggle = function() {
                    self.visible(!self.visible());
                };
            } else {
                self.toggle = function() {
                    self.visible(true);
                };
            }

            function Moment(args) {
                return moment(args).lang("sv");
            }

            function PickerMonth(date) {
                var observableDate = (function () {
                    var v = params.value(), m;
                    if (v) m = Moment(v);
                    if (m && m.isValid()) return m;
                })();

                var manualDate = (date ? Moment(date) : undefined);

                var baseDate = (function() {
                    if (manualDate) return manualDate;
                    else if (observableDate) return observableDate;
                    else return today;
                })();

                var pm = this;

                pm.title = (function(monthName, year) {
                    return monthName.substr(0, 1).toUpperCase() + monthName.substr(1) + ' ' + year;
                })(baseDate.format('MMMM'), baseDate.year());

                pm.prevMonthClick = function () {
                    self.pickerMonth(new PickerMonth(moment(baseDate).subtract('months', 1)));
                };

                pm.nextMonthClick = function () {
                    self.pickerMonth(new PickerMonth(moment(baseDate).add('months', 1)));
                };

                pm.weeks = (function (baseDateMonth, startAt) {
                    function PickerWeek(days) {
                        this.days = days;
                    }

                    function PickerDay(moment) {
                        this.date = moment.date();
                        this.isCurrentMonth = (baseDateMonth == moment.month());
                        this.isToday = today.isSame(moment, 'day');
                        this.isSelected = observableDate && observableDate.isSame(moment, 'day');
                        this.click = function() {
                            if (!params.readOnly) {
                                params.value(moment.format('YYYY-MM-DD'));
                                if (params.value.rcc.accessed) {
                                    params.value.rcc.accessed(true);
                                }
                            }
                            if (params.closable) self.visible(false);
                        };
                    }

                    startAt.subtract('days', startAt.isoWeekday() - 1);
                    return _.map(_.range(6), function(week) {
                        return new PickerWeek(_.map(_.range(7), function(day) {
                            return new PickerDay(moment(startAt).add('days', (week * 7) + day));
                        }));
                    });

                })(baseDate.month(), moment(baseDate).set('date', 1));
            }

            self.pickerMonth(new PickerMonth());

            params.value.subscribe(function (val) {
                if (val && val.match) {
                    var date;
                    if (val.match(/^\d{8}$/)) {
                        date = moment(val, 'YYYYMMDD');
                        if (date.isValid()) {
                            params.value(date.format('YYYY-MM-DD'));
                        }
                    } else  if (val.match(/^\d{6}$/)) {
                        date = moment(val, 'YYMMDD');
                        if (date.isValid()) {
                            if (date.year() > moment().year()) {
                                date.subtract(100, 'years');
                            }
                            params.value(date.format('YYYY-MM-DD'));
                        }
                    }
                    else  if (val.match(/\^d{2}-\d{2}-\d{2}$/)) {
                        date = moment(val, 'YY-MM-DD');
                        if (date.isValid()) {
                            if (date.year() > moment().year()) {
                                date.subtract(100, 'years');
                            }
                            params.value(date.format('YYYY-MM-DD'));
                        }
                    }
                }
                self.pickerMonth(new PickerMonth());
            });
        },
        template: RCC.Vast.Utils.extractSource(function() {/**@preserve
            <div class="rcc-datepicker-wrapper">
                <button class="btn btn-xs btn-default rcc-datepicker-toggle" data-bind="click: toggle, css: { 'active': visible }">
                    <span class="glyphicon glyphicon-calendar"></span>
                </button>
                <div class="rcc-datepicker" data-bind="with: pickerMonth, visible: visible">
                        <!-- ko if: $parent.close -->
                            <div class="div-close">
                                <button data-bind="click: $parent.close" class="close" aria-hidden="true">&times;</button>
                            </div>
                        <!-- /ko -->
                    <table class="table-condensed">
                        <thead>
                            <tr>
                                <th class="prev" data-bind="click: prevMonthClick">«</th>
                                <th class="month" colspan="5" data-bind="text: title"></th>
                                <th class="next" data-bind="click: nextMonthClick">»</th>
                            </tr>
                            <tr data-bind="foreach: ['Må', 'Ti', 'On', 'To', 'Fr', 'Lö', 'Sö']">
                                <th data-bind="text: $data"></th>
                            </tr>
                        </thead>
                        <tbody data-bind="foreach: weeks">
                            <tr data-bind="foreach: days">
                                <td data-bind="text: date, click: click, css: { 'diff-month': !isCurrentMonth, 'today': isToday, 'selected': isSelected }"></td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        */return undefined;})
    });

    ko.bindingHandlers[RCC.bindingsPrefix + 'datepicker'] = {
        init: function (element, valueAccessor, allBindingsAccessor, viewModel, bindingContext) {
            ko.applyBindingsToNode($('<date-picker></date-picker>').insertAfter(element)[0], { component: { name: 'date-picker', params: valueAccessor() } }, bindingContext);
            ko.applyBindingsToNode(element, { 'rcc-value': valueAccessor().value }, bindingContext);
        }
    };

})(jQuery, _, RCC, ko, moment);

(function(inca, RCC, ko, _) {
	ko.components.register('s-a', {
		synchronous: !RCC.Vast.Utils.ieVersion || RCC.Vast.Utils.ieVersion > 8,
		viewModel: function(params) {
			if (!params.value && params.controlType != 'staticText') throw new Error('Parameter "value" is required.');
			var self = this;
			_.extend(self, _.omit(_.defaults(params, {
				question: params.value.rcc.regvar.label,
				info: '',
				controlType: { datetime: 'datepicker', list: 'select', boolean: 'checkbox' }[params.value.rcc && params.value.rcc.term.dataType] || 'text',
				fullWidthText: false,
				placeholder: '',
				enabled: true,
				showReviewerInclude: true,
				tooltip: params.value.rcc.regvar.description,
				dependenciesFulfilled: params.value.rcc.dependenciesFulfilled,
				optionsText: 'text',
                optionsCaption: '– Välj –',
                updateOnKeyPress: false
			}), 'controlType', 'controlChange'));

            self.helpPopover = ko.computed(function() {
                var helpText = params.help || (params.value.rcc && params.value.rcc.documentation && params.value.rcc.documentation() && params.value.rcc.documentation().helpText);
                return helpText ? {
                        title: params.value.rcc.regvar.label,
                        content:  helpText,
                        trigger: 'hover',
                        html: true
                    } : null;
            });

			if (self.showReviewerInclude)
				self.showReviewerInclude = !!(inca.user.role.isReviewer && !inca.form.isReadOnly && self.value.rcc && self.enabled);

		 	self.control = new function() {
				var m = this;
				m.type = (typeof params.controlType == 'string' ? params.controlType : params.controlType.name);
				m.options = _.defaults(params.controlType.options || {}, (function() {
					switch (m.type) {
						case 'datepicker': return { value: self.value, dateToday: inca.serverDate, readOnly: inca.form.isReadOnly || !self.enabled };
						case 'decimal': return { valueObservable: self.value };
						case 'select': return { value: self.value, caption: self.optionsCaption, text: self.optionsText };
						case 'staticText':
							if( _.isFunction(self.value) && _.isObject(self.value()) && self.value().text ){
								return { value: self.value().text };
							}
							return { value: self.value };
						default: return { value: self.value };
					}
				})());
                m.toggleInclude = function () {
                    self.value.rcc.include(!self.value.rcc.include());
                };
		 	};

			self.createControlEvent = function($parent) {
				return new function() {
					if (params.controlChange) this.change = function() { return params.controlChange($parent); };
				};
			};

			self.hasErrors = ko.computed(function () {
				var errors = params.value && params.value.rcc && params.value.rcc.validation && params.value.rcc.validation.errors();
				return errors && errors.length;
			});

			self.hasFatalErrors = ko.computed(function () {
				var errors = params.value && params.value.rcc && params.value.rcc.validation && params.value.rcc.validation.errors();
				return !!(errors && errors.filter(function (e) { return e.fatal; }).length);
			});
		},
		template: RCC.Vast.Utils.extractSource(function() {/**@preserve
			<!-- ko if: dependenciesFulfilled -->
				<div class="single-answer-template r" data-bind="attr: { title: tooltip }">
					<label data-bind="css: { 'accessed': value.rcc && value.rcc.accessed(), 'errors': hasErrors, 'fatal': hasFatalErrors }">
						<span data-bind="popover: helpPopover, html: question + (helpPopover() ? ' <i class=\'fa fa-info-circle\'></i>' : '') + (info ? '<br /><span class=\'help\'>' + info + '</span>' : ''), css: { 'full-width-text': fullWidthText }" class="question"></span>
						<!-- ko if: ko.utils.unwrapObservable($root.showRegvarNames) && value.rcc -->
							<span class="regvar-name" data-bind="text: value.rcc.regvarName"></span>
						<!-- /ko -->
						<!-- ko with: _.extend(control, { event: createControlEvent($parent) }) -->
							<!-- ko if: type == 'select' -->
								<select class="ctrl" data-bind="enable: $parent.enabled, rcc-list: options.value, optionsCaption: options.caption, event: event, optionsText: options.text"></select>
							<!--/ko-->
							<!-- ko if: type == 'text' -->
								 <!-- ko if: $component.updateOnKeyPress -->
								 	<input class="ctrl" type="text" data-bind="rcc-textInput: options.value, event: event, enable: $parent.enabled" />
								 <!--/ko-->
								 <!-- ko ifnot: $component.updateOnKeyPress -->
								 	<input class="ctrl" type="text" data-bind="rcc-value: options.value, event: event, enable: $parent.enabled" />
								 <!--/ko-->
							<!--/ko-->
							<!-- ko if: type == 'datepicker' -->
								<input class="ctrl" type="text" data-bind="rcc-datepicker: options, event: event, enable: $parent.enabled" />
							<!--/ko-->
							<!-- ko if: type == 'staticText' -->
								<!-- ko text: options.value --><!-- /ko -->
							<!--/ko-->
							<!-- ko if: type == 'decimal' -->
								<input class="ctrl" type="text" data-bind="rcc-decimal: options, event: event, enable: $parent.enabled" />
							<!--/ko-->
							<!-- ko if: type == 'textarea' -->
								<textarea class="ctrl" type="text" data-bind="rcc-value: options.value, event: event, enable: $parent.enabled" rows="5"></textarea>
							<!--/ko-->
							<!-- ko if: type == 'checkbox' -->
								<input type="checkbox" data-bind="rcc-checked: options.value, event: event, enable: $parent.enabled">
							<!--/ko-->
							<!-- ko if: $parent.showReviewerInclude -->
								<span class="reviewer-include" title="Inkludera">
                                    <i class="glyphicon" data-bind="click: toggleInclude, css: { 'glyphicon-unchecked': !options.value.rcc.include(), 'glyphicon-check': options.value.rcc.include() }"></i>
								</span>
							<!--/ko-->
						<!--/ko-->
						<!-- ko if: placeholder -->
							<!-- ko text: placeholder --><!--/ko-->
						<!--/ko-->

						<!-- ko if: hasErrors -->
						<div class="error-list" data-bind="foreach: value.rcc.validation.errors" >
							<span class="error-item" data-bind="css: { 'non-fatal': !fatal, 'fatal': fatal }">
								<span class="fa" data-bind="css: { 'fa-exclamation-triangle': !fatal, 'fa-exclamation-circle': fatal }"></span>
								<span data-bind="text: message"></span>
							</span>
						</div>
						<!--/ko-->

					</label>
				</div>
			<!--/ko-->
		*/return undefined;})
	});
})(window['inca'], window['RCC'], window['ko'], window['_']);

(function ($, RCC, inca) {
	var lists =
	{
		JaNej:
		[
			{ text: 'Ja', value: 'ja' },
			{ text: 'Nej', value: 'nej' }
		],

		JaNejIU:
		[
			{ text: 'Ja', value: 'ja' },
			{ text: 'Nej', value: 'nej' },
			{ text: 'Uppgift saknas', value: 'uppgift_saknas' }
		],
		Sjukhus: [
			{ text: 'Arvika', value: 'arvika' },
			{ text: 'Borås', value: 'boras' },
			{ text: 'Danderyd', value: 'danderyd' },
			{ text: 'Eksjö', value: 'eksjo' },
			{ text: 'Eskilstuna', value: 'eskilstuna' },
			{ text: 'Falköping', value: 'falkoping' },
			{ text: 'Falun', value: 'falun' },
			{ text: 'Gällivare', value: 'gallivare' },
			{ text: 'Gävle', value: 'gavle' },
			{ text: 'Halmstad', value: 'halmstad' },
			{ text: 'Helsingborg', value: 'helsingborg' },
			{ text: 'Huddinge', value: 'huddinge' },
			{ text: 'Hudiksvall', value: 'hudiksvall' },
			{ text: 'Hässleholm', value: 'hassleholm' },
			{ text: 'Jönköping-Ryhov', value: 'jonkoping_ryhov' },
			{ text: 'Kalmar', value: 'kalmar' },
			{ text: 'Karlshamn', value: 'karlshamn' },
			{ text: 'Karlskoga', value: 'karlskoga' },
			{ text: 'Karlskrona', value: 'karlskrona' },
			{ text: 'Karlstad', value: 'karlstad' },
			{ text: 'Karlstad', value: 'karlstad' },
			{ text: 'Karolinska, Solna', value: 'karolinska_solna' },
			{ text: 'Katrineholm-Kullbergska', value: 'katrineholm_kullbergska' },
			{ text: 'Kungälv', value: 'kungalv' },
			{ text: 'Landskrona', value: 'landskrona' },
			{ text: 'Lindesberg', value: 'lindesberg' },
			{ text: 'Linköping', value: 'linkoping' },
			{ text: 'Ljungby', value: 'ljungby' },
			{ text: 'Lund', value: 'lund' },
			{ text: 'Löwenströmska', value: 'lowenstromska' },
			{ text: 'Malmö', value: 'malmo' },
			{ text: 'Mora', value: 'mora' },
			{ text: 'Motala', value: 'motala' },
			{ text: 'SU/Mölndal', value: 'su_molndal' },
			{ text: 'Norrköping', value: 'norrkoping' },
			{ text: 'Nyköping', value: 'nykoping' },
			{ text: 'Trollhättan NÄL', value: 'trollhattan_nal' },
			{ text: 'Piteå', value: 'pitea' },
			{ text: 'S:t Göran', value: 'st_goran' },
			{ text: 'SU/Östra sjukhuset', value: 'su_ostra_sjukhuset' },
			{ text: 'SU/Sahlgrenska', value: 'su_sahlgrenska' },
			{ text: 'Skellefteå', value: 'skelleftea' },
			{ text: 'Sollefteå', value: 'solleftea' },
			{ text: 'Sunderbyn', value: 'sunderbyn' },
			{ text: 'Sundsvall', value: 'sundsvall' },
			{ text: 'Säter', value: 'sater' },
			{ text: 'Södersjukhuset', value: 'sodersjukhuset' },
			{ text: 'Södertälje', value: 'sodertalje' },
			{ text: 'Trelleborg', value: 'trelleborg' },
			{ text: 'Uddevalla', value: 'uddevalla' },
			{ text: 'Umeå', value: 'umea' },
			{ text: 'Uppsala', value: 'uppsala' },
			{ text: 'Varberg', value: 'varberg' },
			{ text: 'Visby', value: 'visby' },
			{ text: 'Värnamo', value: 'varnamo' },
			{ text: 'Västervik', value: 'vastervik' },
			{ text: 'Västerås', value: 'vasteras' },
			{ text: 'Växjö', value: 'vaxjo' },
			{ text: 'Ängelholm', value: 'angelholm' },
			{ text: 'Örebro', value: 'orebro' },
			{ text: 'Örnsköldsvik', value: 'ornskoldsvik' },
			{ text: 'Östersund', value: 'ostersund' }
		],
		Aldersgrupp:
		[
			{ text: 'Ålder <= 60', value: 'alderUnder60' },
			{ text: 'Ålder > 60', value: 'alderOver60' },
		],
		
		PulsbreddGrupp:
		[
			{ text: '0,5', value: '0,5' },
			{ text: '1 ', value: '1' },
		],
	};

	// Add dummy ids.
	var n = 0;
	$.each(lists,
		function (unused, list) {
			n++;
			$.each(list,
				function (i, opt) {
					if (!opt.hasOwnProperty('id'))
						opt.id = n * 10000 + i;
				});
		});

	var q = function (x, y) { return x; };

	$.extend(true, inca.form, RCC.Utils.createDummyForm(
	{
		rootTable: 'Random',
		
		regvars:
		{
			randomiseringsdatum: q('datetime', 'NY'),
			pulsbreddgrupp: q(lists.PulsbreddGrupp, 'NY'),
			sjukhus: q(lists.Sjukhus, 'NY'),
			aldersgrupp: q(lists.Aldersgrupp, 'NY'),
			enhet: q('integer', 'NY'),

		},
		subTables:
		{
			
		}
	}));

})(window['jQuery'], window['RCC'], window['inca']);

(function (inca, window) {

	var Utils = {};

	if (!window.RCC)
		window.RCC = {};
	if (!window.RCC.Vast)
		window.RCC.Vast = {};
	if (!window.RCC.Vast.Utils)
		window.RCC.Vast.Utils = {};

	window.RCC.Vast.Utils.Offline = Utils;

	window.queryString = (function () {
		var r = {};
		if (location.search) {
			var col = location.search.substr(1).split('&');
			for (var i = 0; i < col.length; i++) {
				var objName = col[i].split('=')[0];
				if (objName) r[objName] = col[i].substr(objName.length + 1);
			}
		}
		return r;
	})();

	window.showRegvarNames = (window.queryString['regvarNames'] == 'true');

	try
	{
		if (parent != window) parent.SandboxFrame = window;	
	}
	catch (ex) { }
	
	/**
	* Ändrar rottabellen genom att radera angivna tabeller med tillhörande undertabeller. Raderar även värdedomäner
	* som inte hör till den nya rottabellen.
	*
	* @param {String} rootTableName Namn på den nya rottabellen.
	* @param {String[]} tablesToDelete Namn på tabeller som ska raderas. 
	* @param {String[]} [tablesToKeep=[rootTableName]] Namn på tabeller som ska exkluderas.
	*/
	Utils.changeRootTable = function (rootTableName, tablesToDelete, tablesToKeep) {
		if (!tablesToKeep) {
			tablesToKeep = [rootTableName];
		} else if ($.inArray(rootTableName, tablesToKeep) == -1) {
			tablesToKeep.push(rootTableName);
		}

		var toDelete = [];
		function getTableNames(rootTableName) {
			$.each(inca.form.metadata[rootTableName].subTables, function (i, o) {
				if ($.inArray(o, tablesToKeep) == -1) {
					getTableNames(inca.form.metadata[rootTableName].subTables[i]);
				}
			});

			if ($.inArray(rootTableName, toDelete) == -1) {
				toDelete.push(rootTableName);
			}

		}

		$.each(tablesToDelete, function (i, o) { getTableNames(o); });
		$.each(toDelete, function (i, o) { delete inca.form.metadata[o]; });

		$.each(tablesToKeep, function (toKeepIndex, toKeep) {
			var st_del = [];
			$.each(inca.form.metadata[toKeep].subTables, function (subTableIndex, subTable) {
				if ($.inArray(subTable, toDelete) != -1) {
					st_del.push(subTableIndex);
				}
			});
			$.each(st_del, function (i, o) {
				inca.form.metadata[toKeep].subTables.splice(o, 1);
			});
		});

		// Delete all vdlists that do not belong to the root table.
		$.each(inca.form.metadata, function ( tableName, tableMetadata ) {
			if ( tableName !== rootTableName ) {
				tableMetadata.vdlists = {};
			}
		});

		inca.form.data = inca.form.createDataRow(rootTableName);
	};

})(window['inca'], window);

(function() {

	if (queryString.birthDate) {
		var birthDateInt = parseInt(queryString.birthDate);
		if (!isNaN(birthDateInt)) {
			inca.form.env._FODELSEDATUM = RCC.Utils.ISO8601(new Date(birthDateInt));
		}
	}

	

	inca.form.env._SEX = (queryString.gender == 'man' ? 'M' : 'F');

	$(function () {
		$("body").prepend($('<button class="btn btn-primary">Skicka</button>').click(function ( ) { SendForm(); }));
	});
	
})();
(function ($, _) {
	function findGetParam(val) {
		var result = "Not found";
		var tmp = [];
		location.search.substr(1).split("&").forEach(function (item) {
			tmp = item.split("=");
			if (tmp[0] === val) result = decodeURIComponent(tmp[1]);
		});
		return result;
	}
	
	var reloadBtnHolder = null;
	function reloadSticky(){
	    if( reloadBtnHolder ){
	    	$(document.body).trigger("sticky_kit:recalc");
	    }
	}

	function sendIncaObjectToParent(){
		//Skicka till parent (chrome)
		setTimeout(function() {
			var incaData = window.inca;
			//decorate with usage
			var bodyHtml = $("body").html();
			_.map(incaData.form.metadata, function(t,tName){
				t.active = bodyHtml.indexOf(tName) >= 0;
				_.each(t.regvars, function(r,rName){
					r.active = bodyHtml.indexOf(rName) >= 0;
				});
			});
			window.parent.postMessage(JSON.stringify(incaData), "*");
			//window.parent.postMessage(JSON.stringify(vm), "*");
		}, 1000);
	}


	showRegvars = function(e){
		$(this).next(".regvars").toggle();
		reloadSticky();
		return false;
	}

	function showMetadata(metadata){
		var subTables = _.map(metadata, function(o){
			return o.subTables;
		});
		subTables = _.flatten(subTables);
		
		var root = _.difference(_.keys(metadata), subTables);
		if( root.length != 1 ){
			console.error((root.length==0?"Multiple roots":"No root") + " found: "+root);
			return;
		}
		root = root[0];

		var depth = 0;
		var debugMetadataBlock = $("#debugMetadata").find("div");
		function drawTable(tableName){
			var padding = 10*depth;
			var tableRow = $("<div style='padding-left: "+padding+"px;'>"+" <span>"+tableName+"</span></div>");

			var regvars = _.keys(metadata[tableName].regvars);
			if( regvars.length > 0 ){
				var tableSpan = tableRow.find("span");
				tableSpan.on('click', showRegvars);
				tableSpan.css("cursor", "pointer");
				tableSpan.css("color", "darkblue");
				
				var regvarHtml = "";
				_.each(regvars, function(rName){
					var regvar = metadata[tableName].regvars[rName];
					var color = "lightgreen";
					if( !regvar.active ) color = "salmon";
					var type = metadata[tableName].terms[regvar.term].dataType;
					regvarHtml += '<span style="color: '+color+';">'+(regvarHtml==""?'- ':'<br>- ')+rName+" ("+type+")</span>";
				});
				var hiddenRegvars = $("<div class='regvars' style='display: none;'>"+regvarHtml+"</div>");

				tableRow.append(" ("+regvars.length+")");

				tableRow.append(hiddenRegvars);
			}else{
				tableRow.css("color", "salmon");
				tableRow.append(" (0)");
			}


			debugMetadataBlock.append(tableRow);
			depth++;
			_.each(metadata[tableName].subTables, function(t){
				drawTable(t);
			});
			depth--;
		};

		debugMetadataBlock.html("");
		drawTable(root);
	}

	window.updateDebugData = function(inca){
		if( $("#debug").length ){
			if( !$("#debugMetadata").length ) $("#debug").append('<div id="debugMetadata"></div>');

			$("#debugMetadata").html('<h5>Metadata: <a id="mdShow" href="#">show</a> /	<a id="mdHide" href="#">hide</a></h5><div></div>');

			showMetadata(inca.form.metadata);

			$("#mdShow").on('click', function(e){
				e.preventDefault();
				var debugMetadataBlock = $("#debugMetadata").find("div.regvars").show();
				reloadSticky();
				return false;
			});
			$("#mdHide").on('click', function(e){
				e.preventDefault();
				var debugMetadataBlock = $("#debugMetadata").find("div.regvars").hide();
				reloadSticky();
				return false;
			});
		}
	};


	$(function(){
		if( !(document.domain == "localhost" || document.domain == "" || findGetParam("debug") == "true") || findGetParam("debug") == "false") return;

		var url = window.location.pathname;
		var filename = url.substring(url.lastIndexOf('/')+1);
		if( filename == "index.html" ){
			$("body div.l div.w").append('<div class="panel panel-warning"><div class="panel-heading">DEBUG (enbart för systemutvecklare)</div><div id="debug" class="panel-body"></div></div>');

			var debugBtnHolder = $('<div id="debugBtnHolder" class="clearfix"></div>');
			var fullViewBtn = $('<button class="btn btn-primary pull-left">Populate all tables</button>');
			fullViewBtn.on('click', function(e){
				e.preventDefault();
				var iframe = document.getElementsByTagName("iframe")[0];
				iframe.contentWindow.postMessage("populateAllTables", "*");
				return false;
			});
			var reloadMetadataBtn = $('<button class="btn btn-warning pull-right">Reload metadata</button>');
			reloadMetadataBtn.on('click', function(e){
				e.preventDefault();
				var iframe = document.getElementsByTagName("iframe")[0];
				iframe.contentWindow.postMessage("resendIncaObject", "*");
				return false;
			});
			var accessRegvarsBtn = $('<button class="btn btn-info pull-left">Access all regvars</button>');
			accessRegvarsBtn.on('click', function(e){
				e.preventDefault();
				var iframe = document.getElementsByTagName("iframe")[0];
				iframe.contentWindow.postMessage("accessAllRegvars", "*");
				return false;
			});
			debugBtnHolder.append(fullViewBtn);
			debugBtnHolder.append(reloadMetadataBtn);
			debugBtnHolder.append(accessRegvarsBtn);
			$("#debug").append(debugBtnHolder);

			var reloadBtn = $('<button id="reloadBtn" class="btn btn-danger">RELOAD IFRAME</button>');
			reloadBtn.on('click', function(e){
				e.preventDefault();
				var iframe = document.getElementsByTagName("iframe")[0];
				iframe.src = iframe.src;
				return false;
			});
			reloadBtnHolder = $('<div id="reloadBtnHolder" style="float: left;"></div>');
			reloadBtnHolder.append(reloadBtn);
			$("body div.l div.w").append(reloadBtnHolder);
			reloadBtnHolder.stick_in_parent({parent: 'body', offset_top: 100});
			reloadSticky();
		}else if( filename == "formular.html" ){
			sendIncaObjectToParent();
		}
	});

	function listenMessage(msg) {
		if( msg.data == "populateAllTables" ){
			console.log("Populating all tables...");
			(function assertChildrenHaveRows(row){
				$.each( row.$$,
					function ( unused, table )
					{
						if ( table().length == 0 )
							table.rcc.add();

						$.each( table(), function ( unused, childRow ) { assertChildrenHaveRows(childRow); } );
					} );
			})(window.vm);
			sendIncaObjectToParent();
			return;
		}else if( msg.data == "resendIncaObject" ){
			sendIncaObjectToParent();
			return;
		}else if( msg.data == "accessAllRegvars" ){
			function parseStructure(subTables){
				_.each(subTables, function(st, stName){
					if( typeof st == "function" ){
						_.each(st(), function(table){
							parseSubtable(table);
						});
					}else{
						console.error("Subtable not a function: "+stName);
					}
				});
			}
			function parseSubtable(table, excludeSubtables){
				_.each(table, function(prop, propName){
					if( propName == "Nyreg" ){
						parseSubtable(prop, true);
					}

					if( typeof prop == "function" && prop.rcc && prop.rcc.accessed ){
						prop.rcc.accessed(true);
					}

					if( propName == "$$" && !excludeSubtables ){
						parseStructure(prop);
					}
				});
			}
			console.log("Marking all regvars as accessed...");
			parseStructure(window.vm.$$);
			return;
		}

		var inca = JSON.parse(msg.data);
		updateDebugData(inca);
		reloadSticky();
	}

	if (window.addEventListener) {
	    window.addEventListener("message", listenMessage, false);
	} else {
	    window.attachEvent("onmessage", listenMessage);
	}

})(window['jQuery'],window['_']);

function SjukhusModul(observable)
{
	var self = this;
	function sortAZ(arr)
	{
	    arr.sort(function (a, b) {
	        if (a.name < b.name) return -1;
	        if (a.name > b.name) return 1;
	        return 0;
	    })
	}

	self.regioner = (function() {
		var rv = [
		    {
		        name: 'Norra',
		        landsting: [
		            { name: 'Region Västerbotten', sjukhus: ['umea', 'skelleftea'] },
		            { name: 'Region Västernorrland', sjukhus: ['ornskoldsvik', 'sundsvall', 'solleftea'] },
		            { name: 'Region Norrbotten', sjukhus: ['gallivare', 'pitea', 'sunderbyn'] },
		            { name: 'Region Jämtland Härjedalen', sjukhus: ['ostersund'] }
		        ]
		    },
		    {
		        name: 'Uppsala-Örebro',
		        landsting: [
		            { name: 'Region Örebro län', sjukhus: ['orebro', 'karlskoga', 'lindesberg'] },
		            { name: 'Region Uppsala län', sjukhus: ['uppsala'] },
		            { name: 'Region Dalarna', sjukhus: ['sater', 'falun', 'mora'] },
		            { name: 'Region Sörmland', sjukhus: ['nykoping', 'eskilstuna', 'katrineholm_kullbergska'] },
		            { name: 'Region Värmland', sjukhus: ['karlstad', 'arvika'] },
		            { name: 'Region Gävleborg', sjukhus: ['hudiksvall', 'gavle'] },
		            { name: 'Region Västmanland', sjukhus: ['vasteras'] }
		        ]
		    },
		    {
		        name: 'Stockholm',
		        landsting: [
		            {
		                name: 'Region Stockholm',
		                sjukhus: ['lowenstromska', 'danderyd', 'sodertalje', 'st_goran', 'huddinge', 'karolinska_solna', 'sodersjukhuset']
		            },
		            { name: 'Region Gotland', sjukhus: ['visby'] }
		        ]
		    },
		    {
		        name: 'Sydöstra',
		        landsting: [
		            { name: 'Region Östergötland', sjukhus: ['linkoping', 'norrkoping', 'motala'] },
		            { name: 'Region Kalmar län', sjukhus: ['kalmar', 'vastervik'] },
		            { name: 'Region Jönköpings län', sjukhus: ['varnamo', 'eksjo', 'jonkoping_ryhov'] }
		        ]
		    },
		    {
		        name: 'Västra',
		        landsting: [
		            { name: 'Västra Götalandsregionen', sjukhus: ['boras', 'kungalv', 'su_sahlgrenska', 'su_ostra_sjukhuset', 'falkoping', 'su_molndal', 'trollhattan_nal', 'uddevalla'] },
		            { name: 'Region Halland, norra', sjukhus: ['varberg'] }

		        ]
		    },
		    {
		        name: 'Södra',
		        landsting: [
		            { name: 'Region Skåne', sjukhus: ['trelleborg', 'lund', 'malmo', 'kristianstad', 'hassleholm', 'helsingborg', 'angelholm', 'landskrona'] },
		            { name: 'Region Kronoberg', sjukhus: ['vaxjo', 'ljungby'] },
		            { name: 'Region Halland, södra', sjukhus: ['halmstad'] },
		            { name: 'Region Blekinge', sjukhus: ['karlskrona', 'karlshamn'] }
		        ]
		    }
		];

		sortAZ(rv);
		return rv;

	})();
	
	self.visible = ko.observable(false);
	self.selectedRegion = ko.observable();
	self.selectedLandsting = ko.observable();
	self.selectedSjukhus = ko.observable();

	var allaSjukhus = observable.rcc.term.listValues;
	var allaLandsting = (function()
	{
		var rv = [];
		$.each(self.regioner, function (i, o) {
		    $.each(o.landsting, function (ii, oo) {
		        rv.push(oo);
		    });
		});

		sortAZ(rv);
		return rv;
	})();

	self.landsting = ko.computed(function ()
	{
	    var reg = self.selectedRegion();
	    if (reg)
	    {
	        return reg.landsting;
	    }
	    else
	    {
	        return allaLandsting;
	    }
	});

	self.sjukhus = ko.computed(function ()
	{
	    var ls = self.selectedLandsting();
	    if (ls)
	    {
	        return $.grep(allaSjukhus, function (x)
	        {
	            return $.inArray(x.value, ls.sjukhus) != -1;
	        });
	    }
	    else if (self.selectedRegion()) 
	    {
	        var sjukhusIRegion = [];
	        $.each(self.selectedRegion().landsting, function (i, o)
	        {
	            $.each(o.sjukhus, function (ii, oo)
	            {
	                sjukhusIRegion.push(oo);
	            }); 
	        });
	        return $.grep(allaSjukhus, function (x)
	        {
	            return $.inArray(x.value, sjukhusIRegion) != -1;
	        });
	    }
	    else
	    {
	        return allaSjukhus;
	    }
	});

	self.save = function()
	{
		var v = self.selectedSjukhus();
		observable(ko.utils.arrayFirst(allaSjukhus, function (x)
		{
			return x.value == v;
		}));
		self.visible(false);
	};

	observable.subscribe(function ()
	{
	    var v = observable();
	    self.selectedRegion(undefined);
	    self.selectedLandsting(undefined);
	    self.selectedSjukhus(v ? v.value : undefined);
	});
}
var eventHandlers = {},
	subTableConditions;

var randData = [
{"kon":"Kvinna","alder":"18-24","pulsbredd":"0,5","duration":"4","stromstyrka":"800","frekvens":"70","laddning":"224","energi":"45"},
{"kon":"Kvinna","alder":"25-29","pulsbredd":"0,5","duration":"4","stromstyrka":"800","frekvens":"70","laddning":"224","energi":"45"},
{"kon":"Kvinna","alder":"30-34","pulsbredd":"0,5","duration":"6,5","stromstyrka":"800","frekvens":"50","laddning":"260","energi":"50"},
{"kon":"Kvinna","alder":"35-39","pulsbredd":"0,5","duration":"6","stromstyrka":"800","frekvens":"60","laddning":"288","energi":"55"},
{"kon":"Kvinna","alder":"40-44","pulsbredd":"0,5","duration":"6","stromstyrka":"800","frekvens":"60","laddning":"288","energi":"55"},
{"kon":"Kvinna","alder":"45-49","pulsbredd":"0,5","duration":"6","stromstyrka":"800","frekvens":"70","laddning":"336","energi":"65"},
{"kon":"Kvinna","alder":"50-54","pulsbredd":"0,5","duration":"6","stromstyrka":"800","frekvens":"70","laddning":"336","energi":"65"},
{"kon":"Kvinna","alder":"55-59","pulsbredd":"0,5","duration":"6","stromstyrka":"800","frekvens":"70","laddning":"336","energi":"65"},
{"kon":"Kvinna","alder":"60-64","pulsbredd":"0,5","duration":"6","stromstyrka":"800","frekvens":"80","laddning":"384","energi":"75"},
{"kon":"Kvinna","alder":"65-69","pulsbredd":"0,5","duration":"6,5","stromstyrka":"800","frekvens":"70","laddning":"364","energi":"70"},
{"kon":"Kvinna","alder":"70-74","pulsbredd":"0,5","duration":"6","stromstyrka":"800","frekvens":"80","laddning":"384","energi":"75"},
{"kon":"Kvinna","alder":"75-79","pulsbredd":"0,5","duration":"6,5","stromstyrka":"800","frekvens":"80","laddning":"416","energi":"80"},
{"kon":"Kvinna","alder":"80-84","pulsbredd":"0,5","duration":"6,5","stromstyrka":"800","frekvens":"80","laddning":"416","energi":"80"},
{"kon":"Man","alder":"18-24","pulsbredd":"0,5","duration":"6","stromstyrka":"800","frekvens":"50","laddning":"240","energi":"50"},
{"kon":"Man","alder":"25-29","pulsbredd":"0,5","duration":"6,5","stromstyrka":"800","frekvens":"50","laddning":"260","energi":"50"},
{"kon":"Man","alder":"30-34","pulsbredd":"0,5","duration":"6","stromstyrka":"800","frekvens":"60","laddning":"288","energi":"55"},
{"kon":"Man","alder":"35-39","pulsbredd":"0,5","duration":"6","stromstyrka":"800","frekvens":"60","laddning":"288","energi":"55"},
{"kon":"Man","alder":"40-44","pulsbredd":"0,5","duration":"6,5","stromstyrka":"800","frekvens":"60","laddning":"312","energi":"65"},
{"kon":"Man","alder":"45-49","pulsbredd":"0,5","duration":"6","stromstyrka":"800","frekvens":"70","laddning":"336","energi":"65"},
{"kon":"Man","alder":"50-54","pulsbredd":"0,5","duration":"6,5","stromstyrka":"800","frekvens":"70","laddning":"364","energi":"70"},
{"kon":"Man","alder":"55-59","pulsbredd":"0,5","duration":"6,5","stromstyrka":"800","frekvens":"70","laddning":"364","energi":"70"},
{"kon":"Man","alder":"60-64","pulsbredd":"0,5","duration":"6","stromstyrka":"800","frekvens":"80","laddning":"384","energi":"80"},
{"kon":"Man","alder":"65-69","pulsbredd":"0,5","duration":"6,5","stromstyrka":"800","frekvens":"80","laddning":"416","energi":"80"},
{"kon":"Man","alder":"70-74","pulsbredd":"0,5","duration":"7,5","stromstyrka":"800","frekvens":"70","laddning":"420","energi":"85"},
{"kon":"Man","alder":"75-79","pulsbredd":"0,5","duration":"8","stromstyrka":"800","frekvens":"70","laddning":"448","energi":"90"},
{"kon":"Man","alder":"80-84","pulsbredd":"0,5","duration":"7,5","stromstyrka":"800","frekvens":"70","laddning":"420","energi":"85"},
{"kon":"Kvinna","alder":"18-24","pulsbredd":"1","duration":"3","stromstyrka":"800","frekvens":"50","laddning":"240","energi":"45"},
{"kon":"Kvinna","alder":"25-29","pulsbredd":"1","duration":"3","stromstyrka":"800","frekvens":"50","laddning":"240","energi":"45"},
{"kon":"Kvinna","alder":"30-34","pulsbredd":"1","duration":"3","stromstyrka":"800","frekvens":"50","laddning":"240","energi":"50"},
{"kon":"Kvinna","alder":"35-39","pulsbredd":"1","duration":"2,5","stromstyrka":"800","frekvens":"70","laddning":"280","energi":"60"},
{"kon":"Kvinna","alder":"40-44","pulsbredd":"1","duration":"2,5","stromstyrka":"800","frekvens":"70","laddning":"280","energi":"60"},
{"kon":"Kvinna","alder":"45-49","pulsbredd":"1","duration":"3","stromstyrka":"800","frekvens":"70","laddning":"336","energi":"65"},
{"kon":"Kvinna","alder":"50-54","pulsbredd":"1","duration":"3","stromstyrka":"800","frekvens":"70","laddning":"336","energi":"65"},
{"kon":"Kvinna","alder":"55-59","pulsbredd":"1","duration":"3","stromstyrka":"800","frekvens":"70","laddning":"336","energi":"65"},
{"kon":"Kvinna","alder":"60-64","pulsbredd":"1","duration":"3,5","stromstyrka":"800","frekvens":"70","laddning":"392","energi":"75"},
{"kon":"Kvinna","alder":"65-69","pulsbredd":"1","duration":"3","stromstyrka":"800","frekvens":"70","laddning":"336","energi":"70"},
{"kon":"Kvinna","alder":"70-74","pulsbredd":"1","duration":"3,5","stromstyrka":"800","frekvens":"70","laddning":"392","energi":"75"},
{"kon":"Kvinna","alder":"75-79","pulsbredd":"1","duration":"5","stromstyrka":"800","frekvens":"50","laddning":"400","energi":"80"},
{"kon":"Kvinna","alder":"80-84","pulsbredd":"1","duration":"5","stromstyrka":"800","frekvens":"50","laddning":"400","energi":"80"},
{"kon":"Man","alder":"18-24","pulsbredd":"1","duration":"3","stromstyrka":"800","frekvens":"50","laddning":"240","energi":"50"},
{"kon":"Man","alder":"25-29","pulsbredd":"1","duration":"3","stromstyrka":"800","frekvens":"50","laddning":"240","energi":"50"},
{"kon":"Man","alder":"30-34","pulsbredd":"1","duration":"3","stromstyrka":"800","frekvens":"60","laddning":"288","energi":"55"},
{"kon":"Man","alder":"35-39","pulsbredd":"1","duration":"2,5","stromstyrka":"800","frekvens":"70","laddning":"280","energi":"55"},
{"kon":"Man","alder":"40-44","pulsbredd":"1","duration":"3","stromstyrka":"800","frekvens":"70","laddning":"336","energi":"60"},
{"kon":"Man","alder":"45-49","pulsbredd":"1","duration":"3","stromstyrka":"800","frekvens":"70","laddning":"336","energi":"65"},
{"kon":"Man","alder":"50-54","pulsbredd":"1","duration":"3","stromstyrka":"800","frekvens":"70","laddning":"336","energi":"70"},
{"kon":"Man","alder":"55-59","pulsbredd":"1","duration":"3","stromstyrka":"800","frekvens":"70","laddning":"336","energi":"70"},
{"kon":"Man","alder":"60-64","pulsbredd":"1","duration":"4","stromstyrka":"800","frekvens":"60","laddning":"384","energi":"80"},
{"kon":"Man","alder":"65-69","pulsbredd":"1","duration":"5","stromstyrka":"800","frekvens":"50","laddning":"400","energi":"80"},
{"kon":"Man","alder":"70-74","pulsbredd":"1","duration":"5,5","stromstyrka":"800","frekvens":"50","laddning":"440","energi":"85"},
{"kon":"Man","alder":"75-79","pulsbredd":"1","duration":"5,5","stromstyrka":"800","frekvens":"50","laddning":"440","energi":"90"},
{"kon":"Man","alder":"80-84","pulsbredd":"1","duration":"5,5","stromstyrka":"800","frekvens":"50","laddning":"440","energi":"85"}];



var randDataOld = [{"kon":"Man","alder":"85+","pulsbredd":"1","duration":"5,5","stromstyrka":"800","frekvens":"50","laddning":"440","energi":"90"}, {"kon":"Kvinna","alder":"85+","pulsbredd":"1","duration":"5,5","stromstyrka":"800","frekvens":"50","laddning":"440","energi":"85"}, {"kon":"Man","alder":"85+","pulsbredd":"0,5","duration":"8","stromstyrka":"800","frekvens":"70","laddning":"448","energi":"90"}, {"kon":"Kvinna","alder":"85+","pulsbredd":"0,5","duration":"7,5","stromstyrka":"800","frekvens":"70","laddning":"420","energi":"85"}]; 

function getAge() {
	var dateOfBirth = RCC.Utils.parseYMD(inca.form.env._FODELSEDATUM) || RCC.Utils.parseYMD(inca.form.env._PERSNR.slice(0,8));
	var age;
	if (dateOfBirth)
		age = RCC.Vast.Utils.calculateAge(RCC.Utils.parseYMD(inca.form.env._FODELSEDATUM) || RCC.Utils.parseYMD(inca.form.env._PERSNR.slice(0,8)), new Date());
	else
		age = parseInt(prompt('Ange patientens ålder i hela år.', ''));

	if (!(age >= 0))
		throw new Error('Kunde inte avgöra patientens ålder.');
	return age;
}

(function () {
  
	var markAsFatal = RCC.Vast.Utils.markAsFatal,
		addNumMinMaxValidation = RCC.Vast.Utils.addNumMinMaxValidation,
		addYearValidation = RCC.Vast.Utils.addYearValidation;
	

	eventHandlers.rowAdded = {
		Random: function (r) 
		{
			r.sjukhusModul = new SjukhusModul(r.sjukhus);
			if(inca.errand && !inca.form.isReadOnly) {
				r.randomiseringsdatum(moment(inca.serverDate).format('YYYY-MM-DD'));
				var val = getAge() > 60 ? 'alderOver60' : 'alderUnder60';
				r.aldersgrupp(ko.utils.arrayFirst(r.aldersgrupp.rcc.term.listValues, function (list) {
			        return list.value === val;
			    }));
			}
			markAsFatal(r.aldersgrupp);
			markAsFatal(r.sjukhus);
		}
	};
})();

$(function () {

	var actions = { abort: ['Avbryt', 'Radera'], pause: ['Pausa'] };

	try {
		var vm = new RCC.ViewModel({
			validation: new RCC.Validation(),
			events: eventHandlers
		});

		// För offline.
		vm.showRegvarNames = ko.observable(window.showRegvarNames);

		var SendForm = function () {
			var errors = vm.$validation.errors(),
			selectedAction = (function() {
				if (inca.errand && inca.errand.action && inca.errand.action.find)
					return inca.errand.action.find(':selected').text();
			})();

			if ($.inArray(selectedAction, actions.abort) >= 0)
				return true;

			if (errors.length > 0) {
				vm.$validation.markAllAsAccessed();

				var isPause = ($.inArray(selectedAction, actions.pause) >= 0);
				var fatalErrors = $.grep(errors,
					function (error) {
						if (isPause && error.data && error.data.canBeSaved )
							return false;
						else
							return error.fatal;
					});

				if (fatalErrors.length > 0) {
					alert("Formuläret innehåller " + fatalErrors.length + " fel.");
					return false;
				} else if (isPause && fatalErrors.length == 0) return true;

			   //return confirm('Valideringen av frågosvaren gav ' + errors.length + ' varning' + (errors.length == 1 ? '' : 'ar') + '.\r\nVill du gå vidare ändå?');
			}

			return false;
		};


		vm.randDataArr = ko.observableArray([]);
		vm.obsPulsbredd = ko.observable('');
		vm.showRandTable = ko.observable(false);
		vm.isPatientRandomized = ko.observable(false);


	    vm.findRandInfo = function(pulsBredd) {
			var kon = inca.form.env._SEX == 'M' ? 'Man' : 'Kvinna';
			var alder = parseInt(getAge());
			if(alder && kon) {
				if(alder > 84) {
					vm.randDataArr.push(_.find(randDataOld, function(row) {
						return pulsBredd == row.pulsbredd && kon == row.kon;
					}));
				}
				else {
					vm.randDataArr.push(_.find(randData, function(row) {
						var startAge = parseInt(row.alder.substr(0,2));
						var endAge = parseInt(row.alder.substr(3,2));
						return pulsBredd == row.pulsbredd && kon == row.kon && alder >= startAge && alder <= endAge;
					}));
				}
			}
		};

		if(!inca.errand) {
			vm.showRandTable(true);
			vm.findRandInfo(vm.pulsbreddgrupp().value);
		} 
		else {
			inca.form.getValueDomainValues({
				vdlist: 'VD_pulsbredd',
				parameters: { patId: inca.form.env._PATIENTID },
				success: function (list) { if(list[0]) vm.isPatientRandomized(true); }, 
				error: function (err) {console.log(err); }
			});
		}

		vm.getErrand = function() {
			if(vm.isPatientRandomized()) {
				alert('Patienten är redan randomiserad, välj Avbryt!');
				return;
			}
			if(!vm.sjukhus() || !vm.aldersgrupp()) {
				alert('Sjukhus och åldersgrupp måste fyllas i först!');
				return;
			};
			if(getAge() < 18) {
				alert('Patienter under 18 år ska inte ingå i studien');
				return;
			};
			
			$.ajax({
	            url: "https://psykiatri.incanet.se/api/Errands/" + inca.form.data.errandId,
	            type: "GET",
	            contentType: "application/json; charset=utf-8",
	            success: function(data) { vm.executeErrand(data); }, 
	            error: function(err) { 
	            	console.log('fail'); 
	            } 
	        });
		};

		vm.executeErrand = function(errand) {
			if(!errand) {
				console.log('Misslyckades hämta ärende info');
				return;
			}
			$.ajax({
	            url: "https://psykiatri.incanet.se/api/Errands/" + inca.form.data.errandId + "/Event/" + 2 + "/Execute",
	            type: "PUT",
	            contentType: "application/json; charset=utf-8",
	            data: JSON.stringify({ data: inca.form.data, version: errand.version }),
	            success: function(res) {
	                inca.form.getValueDomainValues({
						vdlist: 'VD_pulsbredd',
						parameters: { patId: inca.form.env._PATIENTID },
						success: function (list) { 
							if(list[0] && list[0].data && list[0].data.pulsbreddgrupp_Värde) {
								vm.findRandInfo(list[0].data.pulsbreddgrupp_Värde);
								vm.obsPulsbredd(list[0].data.pulsbreddgrupp_Värde);
								vm.showRandTable(true);
							}
						}, 
						error: function (err) { 
							console.log(err); 
						} 
					});
	            },
	            error: function(err) { 
	            	console.log('fail'); 
	            } 
	        });
		};

		vm.pendingRequests = ko.observable(0);

		inca.on('validation', SendForm);
		// XXX: debug
		window.SendForm = SendForm;

		vm.errors = ko.observableArray([]);
		var ready = ko.observable(false);
		vm.showForm = ko.computed( function ( ) { return ready() && (vm.errors().length == 0 || vm.$form.isReadOnly) && vm.pendingRequests() == 0; } );
		vm.enableHelpText = ko.observable(true);
		ko.applyBindings(vm);
		ready(true);

		// XXX: debug
		window.vm = vm;
	} catch (ex) {
		inca.on("validation", function () {
			if (inca.errand && inca.errand.action && $.inArray(inca.errand.action.find ? inca.errand.action.find(':selected').text() : undefined, actions.abort) != -1) return false;
		});
		alert("Ett fel inträffade\r\n\r\n" + ex + "\r\n" + JSON.stringify(ex));  
	}

});



