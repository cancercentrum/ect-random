(function ($, RCC, inca) {
	var lists =
	{
		JaNej:
		[
			{ text: 'Ja', value: 'ja' },
			{ text: 'Nej', value: 'nej' }
		],

		JaNejIU:
		[
			{ text: 'Ja', value: 'ja' },
			{ text: 'Nej', value: 'nej' },
			{ text: 'Uppgift saknas', value: 'uppgift_saknas' }
		],
		Sjukhus: [
			{ text: 'Arvika', value: 'arvika' },
			{ text: 'Borås', value: 'boras' },
			{ text: 'Danderyd', value: 'danderyd' },
			{ text: 'Eksjö', value: 'eksjo' },
			{ text: 'Eskilstuna', value: 'eskilstuna' },
			{ text: 'Falköping', value: 'falkoping' },
			{ text: 'Falun', value: 'falun' },
			{ text: 'Gällivare', value: 'gallivare' },
			{ text: 'Gävle', value: 'gavle' },
			{ text: 'Halmstad', value: 'halmstad' },
			{ text: 'Helsingborg', value: 'helsingborg' },
			{ text: 'Huddinge', value: 'huddinge' },
			{ text: 'Hudiksvall', value: 'hudiksvall' },
			{ text: 'Hässleholm', value: 'hassleholm' },
			{ text: 'Jönköping-Ryhov', value: 'jonkoping_ryhov' },
			{ text: 'Kalmar', value: 'kalmar' },
			{ text: 'Karlshamn', value: 'karlshamn' },
			{ text: 'Karlskoga', value: 'karlskoga' },
			{ text: 'Karlskrona', value: 'karlskrona' },
			{ text: 'Karlstad', value: 'karlstad' },
			{ text: 'Karlstad', value: 'karlstad' },
			{ text: 'Karolinska, Solna', value: 'karolinska_solna' },
			{ text: 'Katrineholm-Kullbergska', value: 'katrineholm_kullbergska' },
			{ text: 'Kungälv', value: 'kungalv' },
			{ text: 'Landskrona', value: 'landskrona' },
			{ text: 'Lindesberg', value: 'lindesberg' },
			{ text: 'Linköping', value: 'linkoping' },
			{ text: 'Ljungby', value: 'ljungby' },
			{ text: 'Lund', value: 'lund' },
			{ text: 'Löwenströmska', value: 'lowenstromska' },
			{ text: 'Malmö', value: 'malmo' },
			{ text: 'Mora', value: 'mora' },
			{ text: 'Motala', value: 'motala' },
			{ text: 'SU/Mölndal', value: 'su_molndal' },
			{ text: 'Norrköping', value: 'norrkoping' },
			{ text: 'Nyköping', value: 'nykoping' },
			{ text: 'Trollhättan NÄL', value: 'trollhattan_nal' },
			{ text: 'Piteå', value: 'pitea' },
			{ text: 'S:t Göran', value: 'st_goran' },
			{ text: 'SU/Östra sjukhuset', value: 'su_ostra_sjukhuset' },
			{ text: 'SU/Sahlgrenska', value: 'su_sahlgrenska' },
			{ text: 'Skellefteå', value: 'skelleftea' },
			{ text: 'Sollefteå', value: 'solleftea' },
			{ text: 'Sunderbyn', value: 'sunderbyn' },
			{ text: 'Sundsvall', value: 'sundsvall' },
			{ text: 'Säter', value: 'sater' },
			{ text: 'Södersjukhuset', value: 'sodersjukhuset' },
			{ text: 'Södertälje', value: 'sodertalje' },
			{ text: 'Trelleborg', value: 'trelleborg' },
			{ text: 'Uddevalla', value: 'uddevalla' },
			{ text: 'Umeå', value: 'umea' },
			{ text: 'Uppsala', value: 'uppsala' },
			{ text: 'Varberg', value: 'varberg' },
			{ text: 'Visby', value: 'visby' },
			{ text: 'Värnamo', value: 'varnamo' },
			{ text: 'Västervik', value: 'vastervik' },
			{ text: 'Västerås', value: 'vasteras' },
			{ text: 'Växjö', value: 'vaxjo' },
			{ text: 'Ängelholm', value: 'angelholm' },
			{ text: 'Örebro', value: 'orebro' },
			{ text: 'Örnsköldsvik', value: 'ornskoldsvik' },
			{ text: 'Östersund', value: 'ostersund' }
		],
		Aldersgrupp:
		[
			{ text: 'Ålder <= 60', value: 'alderUnder60' },
			{ text: 'Ålder > 60', value: 'alderOver60' },
		],
		
		PulsbreddGrupp:
		[
			{ text: '0,5', value: '0,5' },
			{ text: '1 ', value: '1' },
		],
	};

	// Add dummy ids.
	var n = 0;
	$.each(lists,
		function (unused, list) {
			n++;
			$.each(list,
				function (i, opt) {
					if (!opt.hasOwnProperty('id'))
						opt.id = n * 10000 + i;
				});
		});

	var q = function (x, y) { return x; };

	$.extend(true, inca.form, RCC.Utils.createDummyForm(
	{
		rootTable: 'Random',
		
		regvars:
		{
			randomiseringsdatum: q('datetime', 'NY'),
			pulsbreddgrupp: q(lists.PulsbreddGrupp, 'NY'),
			sjukhus: q(lists.Sjukhus, 'NY'),
			aldersgrupp: q(lists.Aldersgrupp, 'NY'),
			enhet: q('integer', 'NY'),

		},
		subTables:
		{
			
		}
	}));

})(window['jQuery'], window['RCC'], window['inca']);
