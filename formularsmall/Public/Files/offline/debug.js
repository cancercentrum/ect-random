(function ($, _) {
	function findGetParam(val) {
		var result = "Not found";
		var tmp = [];
		location.search.substr(1).split("&").forEach(function (item) {
			tmp = item.split("=");
			if (tmp[0] === val) result = decodeURIComponent(tmp[1]);
		});
		return result;
	}
	
	var reloadBtnHolder = null;
	function reloadSticky(){
	    if( reloadBtnHolder ){
	    	$(document.body).trigger("sticky_kit:recalc");
	    }
	}

	function sendIncaObjectToParent(){
		//Skicka till parent (chrome)
		setTimeout(function() {
			var incaData = window.inca;
			//decorate with usage
			var bodyHtml = $("body").html();
			_.map(incaData.form.metadata, function(t,tName){
				t.active = bodyHtml.indexOf(tName) >= 0;
				_.each(t.regvars, function(r,rName){
					r.active = bodyHtml.indexOf(rName) >= 0;
				});
			});
			window.parent.postMessage(JSON.stringify(incaData), "*");
			//window.parent.postMessage(JSON.stringify(vm), "*");
		}, 1000);
	}


	showRegvars = function(e){
		$(this).next(".regvars").toggle();
		reloadSticky();
		return false;
	}

	function showMetadata(metadata){
		var subTables = _.map(metadata, function(o){
			return o.subTables;
		});
		subTables = _.flatten(subTables);
		
		var root = _.difference(_.keys(metadata), subTables);
		if( root.length != 1 ){
			console.error((root.length==0?"Multiple roots":"No root") + " found: "+root);
			return;
		}
		root = root[0];

		var depth = 0;
		var debugMetadataBlock = $("#debugMetadata").find("div");
		function drawTable(tableName){
			var padding = 10*depth;
			var tableRow = $("<div style='padding-left: "+padding+"px;'>"+" <span>"+tableName+"</span></div>");

			var regvars = _.keys(metadata[tableName].regvars);
			if( regvars.length > 0 ){
				var tableSpan = tableRow.find("span");
				tableSpan.on('click', showRegvars);
				tableSpan.css("cursor", "pointer");
				tableSpan.css("color", "darkblue");
				
				var regvarHtml = "";
				_.each(regvars, function(rName){
					var regvar = metadata[tableName].regvars[rName];
					var color = "lightgreen";
					if( !regvar.active ) color = "salmon";
					var type = metadata[tableName].terms[regvar.term].dataType;
					regvarHtml += '<span style="color: '+color+';">'+(regvarHtml==""?'- ':'<br>- ')+rName+" ("+type+")</span>";
				});
				var hiddenRegvars = $("<div class='regvars' style='display: none;'>"+regvarHtml+"</div>");

				tableRow.append(" ("+regvars.length+")");

				tableRow.append(hiddenRegvars);
			}else{
				tableRow.css("color", "salmon");
				tableRow.append(" (0)");
			}


			debugMetadataBlock.append(tableRow);
			depth++;
			_.each(metadata[tableName].subTables, function(t){
				drawTable(t);
			});
			depth--;
		};

		debugMetadataBlock.html("");
		drawTable(root);
	}

	window.updateDebugData = function(inca){
		if( $("#debug").length ){
			if( !$("#debugMetadata").length ) $("#debug").append('<div id="debugMetadata"></div>');

			$("#debugMetadata").html('<h5>Metadata: <a id="mdShow" href="#">show</a> /	<a id="mdHide" href="#">hide</a></h5><div></div>');

			showMetadata(inca.form.metadata);

			$("#mdShow").on('click', function(e){
				e.preventDefault();
				var debugMetadataBlock = $("#debugMetadata").find("div.regvars").show();
				reloadSticky();
				return false;
			});
			$("#mdHide").on('click', function(e){
				e.preventDefault();
				var debugMetadataBlock = $("#debugMetadata").find("div.regvars").hide();
				reloadSticky();
				return false;
			});
		}
	};


	$(function(){
		if( !(document.domain == "localhost" || document.domain == "" || findGetParam("debug") == "true") || findGetParam("debug") == "false") return;

		var url = window.location.pathname;
		var filename = url.substring(url.lastIndexOf('/')+1);
		if( filename == "index.html" ){
			$("body div.l div.w").append('<div class="panel panel-warning"><div class="panel-heading">DEBUG (enbart för systemutvecklare)</div><div id="debug" class="panel-body"></div></div>');

			var debugBtnHolder = $('<div id="debugBtnHolder" class="clearfix"></div>');
			var fullViewBtn = $('<button class="btn btn-primary pull-left">Populate all tables</button>');
			fullViewBtn.on('click', function(e){
				e.preventDefault();
				var iframe = document.getElementsByTagName("iframe")[0];
				iframe.contentWindow.postMessage("populateAllTables", "*");
				return false;
			});
			var reloadMetadataBtn = $('<button class="btn btn-warning pull-right">Reload metadata</button>');
			reloadMetadataBtn.on('click', function(e){
				e.preventDefault();
				var iframe = document.getElementsByTagName("iframe")[0];
				iframe.contentWindow.postMessage("resendIncaObject", "*");
				return false;
			});
			var accessRegvarsBtn = $('<button class="btn btn-info pull-left">Access all regvars</button>');
			accessRegvarsBtn.on('click', function(e){
				e.preventDefault();
				var iframe = document.getElementsByTagName("iframe")[0];
				iframe.contentWindow.postMessage("accessAllRegvars", "*");
				return false;
			});
			debugBtnHolder.append(fullViewBtn);
			debugBtnHolder.append(reloadMetadataBtn);
			debugBtnHolder.append(accessRegvarsBtn);
			$("#debug").append(debugBtnHolder);

			var reloadBtn = $('<button id="reloadBtn" class="btn btn-danger">RELOAD IFRAME</button>');
			reloadBtn.on('click', function(e){
				e.preventDefault();
				var iframe = document.getElementsByTagName("iframe")[0];
				iframe.src = iframe.src;
				return false;
			});
			reloadBtnHolder = $('<div id="reloadBtnHolder" style="float: left;"></div>');
			reloadBtnHolder.append(reloadBtn);
			$("body div.l div.w").append(reloadBtnHolder);
			reloadBtnHolder.stick_in_parent({parent: 'body', offset_top: 100});
			reloadSticky();
		}else if( filename == "formular.html" ){
			sendIncaObjectToParent();
		}
	});

	function listenMessage(msg) {
		if( msg.data == "populateAllTables" ){
			console.log("Populating all tables...");
			(function assertChildrenHaveRows(row){
				$.each( row.$$,
					function ( unused, table )
					{
						if ( table().length == 0 )
							table.rcc.add();

						$.each( table(), function ( unused, childRow ) { assertChildrenHaveRows(childRow); } );
					} );
			})(window.vm);
			sendIncaObjectToParent();
			return;
		}else if( msg.data == "resendIncaObject" ){
			sendIncaObjectToParent();
			return;
		}else if( msg.data == "accessAllRegvars" ){
			function parseStructure(subTables){
				_.each(subTables, function(st, stName){
					if( typeof st == "function" ){
						_.each(st(), function(table){
							parseSubtable(table);
						});
					}else{
						console.error("Subtable not a function: "+stName);
					}
				});
			}
			function parseSubtable(table, excludeSubtables){
				_.each(table, function(prop, propName){
					if( propName == "Nyreg" ){
						parseSubtable(prop, true);
					}

					if( typeof prop == "function" && prop.rcc && prop.rcc.accessed ){
						prop.rcc.accessed(true);
					}

					if( propName == "$$" && !excludeSubtables ){
						parseStructure(prop);
					}
				});
			}
			console.log("Marking all regvars as accessed...");
			parseStructure(window.vm.$$);
			return;
		}

		var inca = JSON.parse(msg.data);
		updateDebugData(inca);
		reloadSticky();
	}

	if (window.addEventListener) {
	    window.addEventListener("message", listenMessage, false);
	} else {
	    window.attachEvent("onmessage", listenMessage);
	}

})(window['jQuery'],window['_']);
