window.inca =
{
	user: { role: { isReviewer: false }, region: { id: -1, name: 'Väst' }, position: { id: -1 } },
	on: function ( ) { if ( window.console ) console.warn( 'inca.on not implemented' ); },
	form:
	{
		isReadOnly: false,
		getRegisterRecordData:
			function ( )
			{
				throw new Error('unimplemented');
			},
		getValueDomainValues:
			function ( opt )
			{
				if ( opt.success )
				{
					setTimeout(
						function ( )
						{
							switch ( opt.vdlist )
							{
								/*
								case 'VD_NjureProcess_Uppf':
									opt.success( [{"text":"x","id":38,"data":{"R1490T17362_ID":38,"R92T322_18112_RappDat":"2012-01-01","R1490T17362_ID2":38,"uppdaterad":"2012-10-30"}},{"text":"x","id":58,"data":{"R1490T17362_ID":58,"R92T322_18112_RappDat":"2012-01-01","R1490T17362_ID2":58,"uppdaterad":"2012-11-09"}}] );
									break;
								*/
								case 'VD_Registernamn_Nyreg':
									opt.success( [{"text":"x","id":4,"data":{"inregistreringsdatum":"2014-01-05"}},{"text":"x","id":49,"data":{"inregistreringsdatum":"2013-11-05"}}] );
									break;
							}
						}, (1+Math.random())*250 ); 
				}
			}
	},
	errand: { status: { val: function () { return 'Nytt ärende'; } } }
};
