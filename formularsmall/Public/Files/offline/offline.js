(function() {

	if (queryString.birthDate) {
		var birthDateInt = parseInt(queryString.birthDate);
		if (!isNaN(birthDateInt)) {
			inca.form.env._FODELSEDATUM = RCC.Utils.ISO8601(new Date(birthDateInt));
		}
	}

	

	inca.form.env._SEX = (queryString.gender == 'man' ? 'M' : 'F');

	$(function () {
		$("body").prepend($('<button class="btn btn-primary">Skicka</button>').click(function ( ) { SendForm(); }));
	});
	
})();