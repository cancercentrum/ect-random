/**
*
*/

(function(inca, RCC, ko, _) {
	ko.components.register('sjukhus', {
		synchronous: !RCC.Vast.Utils.ieVersion || RCC.Vast.Utils.ieVersion > 8,
		viewModel: function(params) {
			var self = this;
			self.inrappEnhet = params.inrappEnhet;
			self.sjukhusKod = params.sjukhusKod;
			self.klinikKod = params.klinikKod;
			self.vdList = params.vdList;
			self.patCyt = params.patCyt || false;


			self.autoComplete = {
				searchValueObservable: ko.observable(''),
				filter: function(x) {					
					return RCC.AutoCompleteFilters.containsEachWord(this.searchValueObservable(), x.data.PosNameWithCode);
				},

				format: function(x) {
					return '<b>' + x.data.PosNameWithCode + '</b> ';
				},

				selectRow: function(x) {
					self.inrappEnhet.regvar(x.data.data.PosNameWithCode);
					self.parseCodes(x.data.data.PosCode);
					this.searchValueObservable(self.inrappEnhet.regvar());
				},

				data: ko.observable(undefined),
				selectOnTab: true
			};
		

			self.parseCodes = function(code) {
				if(!self.inrappEnhet.regvar() || !code) 
					return;
				var splitCodes = code.split('-');
				if(self.sjukhusKod && splitCodes[1])
					self.sjukhusKod.regvar(splitCodes[1].trim());
				if(self.klinikKod && splitCodes[2])
					self.klinikKod.regvar(splitCodes[2].trim());
			};

			self.isValidCode = ko.observable();

			self.isValidCode.subscribe(function() {
				var res = _.find(self.autoComplete.data(), function(obj) {
					return self.inrappEnhet.regvar() && self.inrappEnhet.regvar() == obj.data.PosNameWithCode;
				});
				if(!res)
					self.autoComplete.searchValueObservable('');
			});

			self.removeTopCodes = function() {
				var res = _.reject(self.autoComplete.data(), function(obj) {
					return !obj.data.PosNameWithCode.match(/-/);
				});
				self.autoComplete.data(res);
			};

			self.setReporterCode = function() {
				var res = _.find(self.autoComplete.data(), function(obj) {
					return obj.data.PosNameWithCode && inca.user.position.fullNameWithCode && obj.data.PosNameWithCode == inca.user.position.fullNameWithCode;
				});
				
				if(res) {
					self.inrappEnhet.regvar(res.data.PosNameWithCode);
					self.autoComplete.searchValueObservable(res.data.PosNameWithCode);
				}
			};

			self.filterPatCytCodes = function() {
				if(self.autoComplete.data().length == 0)
					return;
				self.autoComplete.data(_.filter(self.autoComplete.data(), function(obj) {
					var x = obj.data.PosCode.split(" - ");
					return x.length == 2 && x[1].length == 3;
				}));
			};


			inca.form.getValueDomainValues({
				vdlist: self.vdList,
				success: function(list) {
					self.autoComplete.data(list);
					self.removeTopCodes();
					if(self.patCyt)
						self.filterPatCytCodes();
		            var res = _.find(self.autoComplete.data(), function(obj) {
						return self.inrappEnhet.regvar() && self.inrappEnhet.regvar() == obj.data.PosNameWithCode;
					});

		            if(res)
		            	self.parseCodes(res.data.PosCode);

					if(self.inrappEnhet.regvar())
            			self.autoComplete.searchValueObservable(self.inrappEnhet.regvar());
				},
				error: function(err) {}
			});


			self.hasErrors = ko.computed(function () {
				var errors = self.inrappEnhet.regvar && self.inrappEnhet.regvar.rcc && self.inrappEnhet.regvar.rcc.validation && self.inrappEnhet.regvar.rcc.validation.errors();
				return errors && errors.length;
			});

			self.hasFatalErrors = ko.computed(function () {
				var errors = self.inrappEnhet.regvar && self.inrappEnhet.regvar.rcc && self.inrappEnhet.regvar.rcc.validation && self.inrappEnhet.regvar.rcc.validation.errors();
				return !!(errors && errors.filter(function (e) { return e.fatal; }).length);
			});

			

			self.helpPopover = ko.computed(function() {
                var helpText = self.inrappEnhet.help || (self.inrappEnhet.regvar.rcc && self.inrappEnhet.regvar.rcc.documentation && self.inrappEnhet.regvar.rcc.documentation() && self.inrappEnhet.regvar.rcc.documentation().helpText);
                return helpText ? {
                        title: self.inrappEnhet.regvar.rcc.regvar.label,
                        content:  helpText,
                        trigger: 'hover',
                        html: true
                    } : null;
            });

			self.getLabelText = function(data) {
                if(!data.label && self.inrappEnhet.regvar.rcc && self.inrappEnhet.regvar.rcc.regvar)
                    return self.inrappEnhet.regvar.rcc.regvar.label;
                else 
                    return self.inrappEnhet.label; 
            };
		},
		template: RCC.Vast.Utils.extractSource(function() {/**@preserve
			<div class="single-answer-template r">
				<label data-bind="css: { 'accessed': inrappEnhet.regvar.rcc && inrappEnhet.regvar.rcc.accessed(), 'errors': hasErrors, 'fatal': hasFatalErrors }">
					<span class="question" data-bind="popover: helpPopover, html: getLabelText(inrappEnhet) + ' <i class=\'fa fa-info-circle\'></i>'"><span></span></span>
					<input class="form-control" style="display: block; width: 560px; margin-top: 5px" type="text" data-bind="hasFocus: isValidCode, rcc-autocomplete: autoComplete, value: autoComplete.searchValueObservable, rcc-var: inrappEnhet.regvar, valueUpdate: 'afterkeydown'" />
					<!-- ko if: hasErrors -->
						<div class="error-list" data-bind="foreach: inrappEnhet.regvar.rcc.validation.errors" >
							<span class="error-item" data-bind="css: { 'non-fatal': !fatal, 'fatal': fatal }">
								<span class="fa" data-bind="css: { 'fa-exclamation-triangle': !fatal, 'fa-exclamation-circle': fatal }"></span>
								<span data-bind="text: message"></span>
							</span>
						</div>
					<!--/ko-->
				</label>
			</div>
			<!-- ko if: inca.user.role.isReviewer -->
				<div style="color: mediumblue">
				<!-- ko if: sjukhusKod && ko.isObservable(sjukhusKod.regvar) -->
					<s-a params="question: sjukhusKod.label, value: sjukhusKod.regvar, help: sjukhusKod.help"></s-a>
				<!-- /ko -->
				<!-- ko if: klinikKod && ko.isObservable(klinikKod.regvar) -->
					<s-a params="question: klinikKod.label, value: klinikKod.regvar, help: klinikKod.help"></s-a>
				<!-- /ko -->
				</div>
			<!--/ko -->
		*/return undefined;})
	});
})(window['inca'], window['RCC'], window['ko'], window['_']);