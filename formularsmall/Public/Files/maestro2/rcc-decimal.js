/**
* rcc-decimal
* Användning:
*   data-bind="rcc-decimal: valueObservable"
*   data-bind="rcc-decimal: { @param: värde }"
*
* @param {ko.observable} valueObservable Datakoppling.
* @param {Boolean} [allowPointAsDecimalSeparator=false] Ersätter punkter med kommatecken.
* @param {Boolean} [padWithZeros=true] Automatisk komplettering med nollor för att matcha antalet decimaler i variabeln.
*/
(function (RCC, ko, _) {
    ko.bindingHandlers[RCC.bindingsPrefix + 'decimal'] = {
        init: function (element, valueAccessor, allBindingsAccessor, viewModel, bindingContext) {
            var val = valueAccessor(),
                opt = _.defaults((ko.isObservable(val) ? { valueObservable: val } : val), { allowPointAsDecimalSeparator: false, padWithZeros: true }),
                metadata = opt.valueObservable[RCC.metadataProperty];

            if (!metadata || !metadata.term || metadata.term.dataType != "decimal")
                throw new Error('The binding "' + RCC.bindingsPrefix + 'decimal" requires a decimal observable from RCC.ViewModel as argument.');

            var precision = opt.valueObservable[RCC.metadataProperty].term.precision;
            var bindings = {
                event: {
                    change: function () {
                        var value = opt.valueObservable();
                        if (value) {
                            if (opt.allowPointAsDecimalSeparator) value = value.replace(/\./g, ',');
                            if (opt.padWithZeros && new RegExp("^-?(?:\\d+|\\d*,\\d{1," + precision + "})$").exec(value))
                                value = parseFloat(value.replace(/,/g, '.')).toFixed(precision).toString().replace(/\./g, ',');
                            opt.valueObservable(value);
                        }
                        return true;
                    }
                }
            };
            var valueBinding = {};
            valueBinding[RCC.bindingsPrefix + 'value'] = opt.valueObservable;
            ko.applyBindingsToNode(element, valueBinding, bindingContext);
            ko.applyBindingsToNode(element, bindings, bindingContext);
        }
    };
})(window['RCC'], window['ko'], window['_']);