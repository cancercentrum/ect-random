/**
* DatePicker
* Användning:
*   <date-picker params="@param: värde"></date-picker>
*   <input type="text" data-bind="rcc-datepicker: { @param: värde }" />
*
* @param {ko.observable} value Datakoppling.
* @param {Date} [dateToday=new Date()] Dagens datum.
* @param {Boolean} [autoOpen=false] Visar datumväljaren automatiskt från start.
* @param {Boolean} [closable=true] Anger huruvida datumväljaren ska kunna stängas.
* @param {Boolean} [readOnly=false] Tillåt endast visning av värdet.
*/
(function ($, _, RCC, ko, moment) {
    ko.components.register('date-picker', {
        synchronous: !RCC.Vast.Utils.ieVersion || RCC.Vast.Utils.ieVersion > 8,
        viewModel: function(params) {
            if (!params.value) throw new Error('Parameter "value" is required.');
            _.defaults(params, { autoOpen: false, closable: true, readOnly: false });

            var self = this, today = (params.dateToday ? Moment(params.dateToday) : Moment());
            self.visible = ko.observable(params.autoOpen);
            self.pickerMonth = ko.observable();

            if (params.closable) {
                self.close = function() {
                    self.visible(false);
                };
                self.toggle = function() {
                    self.visible(!self.visible());
                };
            } else {
                self.toggle = function() {
                    self.visible(true);
                };
            }

            function Moment(args) {
                return moment(args).lang("sv");
            }

            function PickerMonth(date) {
                var observableDate = (function () {
                    var v = params.value(), m;
                    if (v) m = Moment(v);
                    if (m && m.isValid()) return m;
                })();

                var manualDate = (date ? Moment(date) : undefined);

                var baseDate = (function() {
                    if (manualDate) return manualDate;
                    else if (observableDate) return observableDate;
                    else return today;
                })();

                var pm = this;

                pm.title = (function(monthName, year) {
                    return monthName.substr(0, 1).toUpperCase() + monthName.substr(1) + ' ' + year;
                })(baseDate.format('MMMM'), baseDate.year());

                pm.prevMonthClick = function () {
                    self.pickerMonth(new PickerMonth(moment(baseDate).subtract('months', 1)));
                };

                pm.nextMonthClick = function () {
                    self.pickerMonth(new PickerMonth(moment(baseDate).add('months', 1)));
                };

                pm.weeks = (function (baseDateMonth, startAt) {
                    function PickerWeek(days) {
                        this.days = days;
                    }

                    function PickerDay(moment) {
                        this.date = moment.date();
                        this.isCurrentMonth = (baseDateMonth == moment.month());
                        this.isToday = today.isSame(moment, 'day');
                        this.isSelected = observableDate && observableDate.isSame(moment, 'day');
                        this.click = function() {
                            if (!params.readOnly) {
                                params.value(moment.format('YYYY-MM-DD'));
                                if (params.value.rcc.accessed) {
                                    params.value.rcc.accessed(true);
                                }
                            }
                            if (params.closable) self.visible(false);
                        };
                    }

                    startAt.subtract('days', startAt.isoWeekday() - 1);
                    return _.map(_.range(6), function(week) {
                        return new PickerWeek(_.map(_.range(7), function(day) {
                            return new PickerDay(moment(startAt).add('days', (week * 7) + day));
                        }));
                    });

                })(baseDate.month(), moment(baseDate).set('date', 1));
            }

            self.pickerMonth(new PickerMonth());

            params.value.subscribe(function (val) {
                if (val && val.match) {
                    var date;
                    if (val.match(/^\d{8}$/)) {
                        date = moment(val, 'YYYYMMDD');
                        if (date.isValid()) {
                            params.value(date.format('YYYY-MM-DD'));
                        }
                    } else  if (val.match(/^\d{6}$/)) {
                        date = moment(val, 'YYMMDD');
                        if (date.isValid()) {
                            if (date.year() > moment().year()) {
                                date.subtract(100, 'years');
                            }
                            params.value(date.format('YYYY-MM-DD'));
                        }
                    }
                    else  if (val.match(/\^d{2}-\d{2}-\d{2}$/)) {
                        date = moment(val, 'YY-MM-DD');
                        if (date.isValid()) {
                            if (date.year() > moment().year()) {
                                date.subtract(100, 'years');
                            }
                            params.value(date.format('YYYY-MM-DD'));
                        }
                    }
                }
                self.pickerMonth(new PickerMonth());
            });
        },
        template: RCC.Vast.Utils.extractSource(function() {/**@preserve
            <div class="rcc-datepicker-wrapper">
                <button class="btn btn-xs btn-default rcc-datepicker-toggle" data-bind="click: toggle, css: { 'active': visible }">
                    <span class="glyphicon glyphicon-calendar"></span>
                </button>
                <div class="rcc-datepicker" data-bind="with: pickerMonth, visible: visible">
                        <!-- ko if: $parent.close -->
                            <div class="div-close">
                                <button data-bind="click: $parent.close" class="close" aria-hidden="true">&times;</button>
                            </div>
                        <!-- /ko -->
                    <table class="table-condensed">
                        <thead>
                            <tr>
                                <th class="prev" data-bind="click: prevMonthClick">«</th>
                                <th class="month" colspan="5" data-bind="text: title"></th>
                                <th class="next" data-bind="click: nextMonthClick">»</th>
                            </tr>
                            <tr data-bind="foreach: ['Må', 'Ti', 'On', 'To', 'Fr', 'Lö', 'Sö']">
                                <th data-bind="text: $data"></th>
                            </tr>
                        </thead>
                        <tbody data-bind="foreach: weeks">
                            <tr data-bind="foreach: days">
                                <td data-bind="text: date, click: click, css: { 'diff-month': !isCurrentMonth, 'today': isToday, 'selected': isSelected }"></td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        */return undefined;})
    });

    ko.bindingHandlers[RCC.bindingsPrefix + 'datepicker'] = {
        init: function (element, valueAccessor, allBindingsAccessor, viewModel, bindingContext) {
            ko.applyBindingsToNode($('<date-picker></date-picker>').insertAfter(element)[0], { component: { name: 'date-picker', params: valueAccessor() } }, bindingContext);
            ko.applyBindingsToNode(element, { 'rcc-value': valueAccessor().value }, bindingContext);
        }
    };

})(jQuery, _, RCC, ko, moment);
