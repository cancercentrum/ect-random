﻿(function (inca, window) {

	var Utils = {};

	if (!window.RCC)
		window.RCC = {};
	if (!window.RCC.Vast)
		window.RCC.Vast = {};
	if (!window.RCC.Vast.Utils)
		window.RCC.Vast.Utils = {};

	window.RCC.Vast.Utils.Offline = Utils;

	window.queryString = (function () {
		var r = {};
		if (location.search) {
			var col = location.search.substr(1).split('&');
			for (var i = 0; i < col.length; i++) {
				var objName = col[i].split('=')[0];
				if (objName) r[objName] = col[i].substr(objName.length + 1);
			}
		}
		return r;
	})();

	window.showRegvarNames = (window.queryString['regvarNames'] == 'true');

	try
	{
		if (parent != window) parent.SandboxFrame = window;	
	}
	catch (ex) { }
	
	/**
	* Ändrar rottabellen genom att radera angivna tabeller med tillhörande undertabeller. Raderar även värdedomäner
	* som inte hör till den nya rottabellen.
	*
	* @param {String} rootTableName Namn på den nya rottabellen.
	* @param {String[]} tablesToDelete Namn på tabeller som ska raderas. 
	* @param {String[]} [tablesToKeep=[rootTableName]] Namn på tabeller som ska exkluderas.
	*/
	Utils.changeRootTable = function (rootTableName, tablesToDelete, tablesToKeep) {
		if (!tablesToKeep) {
			tablesToKeep = [rootTableName];
		} else if ($.inArray(rootTableName, tablesToKeep) == -1) {
			tablesToKeep.push(rootTableName);
		}

		var toDelete = [];
		function getTableNames(rootTableName) {
			$.each(inca.form.metadata[rootTableName].subTables, function (i, o) {
				if ($.inArray(o, tablesToKeep) == -1) {
					getTableNames(inca.form.metadata[rootTableName].subTables[i]);
				}
			});

			if ($.inArray(rootTableName, toDelete) == -1) {
				toDelete.push(rootTableName);
			}

		}

		$.each(tablesToDelete, function (i, o) { getTableNames(o); });
		$.each(toDelete, function (i, o) { delete inca.form.metadata[o]; });

		$.each(tablesToKeep, function (toKeepIndex, toKeep) {
			var st_del = [];
			$.each(inca.form.metadata[toKeep].subTables, function (subTableIndex, subTable) {
				if ($.inArray(subTable, toDelete) != -1) {
					st_del.push(subTableIndex);
				}
			});
			$.each(st_del, function (i, o) {
				inca.form.metadata[toKeep].subTables.splice(o, 1);
			});
		});

		// Delete all vdlists that do not belong to the root table.
		$.each(inca.form.metadata, function ( tableName, tableMetadata ) {
			if ( tableName !== rootTableName ) {
				tableMetadata.vdlists = {};
			}
		});

		inca.form.data = inca.form.createDataRow(rootTableName);
	};

})(window['inca'], window);
