(function (inca, RCC, ko, _) {
    ko.components.register('table-multi-answer-question', {
        synchronous: !RCC.Vast.Utils.ieVersion || RCC.Vast.Utils.ieVersion > 8,
        viewModel: function (params) {
            if (!params.table) {
                return;
            }

            var self = this;
            self.table = params.table;
            self.columns = params.columns;
            self.extraRows = params.extraRows || [];
            self.addRowText = params.addRowText || 'Lägg till';
            self.canRemoveExistingRows = params.canRemoveExistingRows !== false;
            self.minRow = params.minRow || 0;
            self.baseQuestion = ko.observable();
            self.isReviewer = inca.user.role.isReviewer;
            self.components = params.components || [];
            self.expandedContent = params.expandedContent || [];
            self.isRegisterRecord = params.registerrecord || false;

            if (self.table().length) {
                var firstTableRow = self.table()[0];
                self.expandedContent.forEach(function (row) {
                    if (row.regvar && !firstTableRow[row.regvar]) {
                        console.warn('invalid expanded content regvar', row.regvar);
                    }
                });
            }
            var setExpansionFunctions = function (tablerow) {
                if (!tablerow.isExpanded) {
                    tablerow.isExpanded = ko.observable(false);
                }

                if (!tablerow.toggleExpand) {
                    tablerow.toggleExpand = function () {
                        tablerow.isExpanded(!tablerow.isExpanded());
                    }
                }
            };
            self.table().forEach(setExpansionFunctions);
            self.table.subscribe(function (rows) {
                rows.forEach(setExpansionFunctions);
            });

            self.fullColspan = ko.computed(function () {
                var extraColumns = 1;

                if (self.expandedContent.length) {
                    extraColumns++;
                }

                return self.columns.length + extraColumns;
            });

            self.getMetadataLabel = function(field) {
                for(var table in inca.form.metadata) {
                    for(var regvar in inca.form.metadata[table].regvars) {
                        if(regvar == field && inca.form.metadata[table].regvars[regvar].label)
                            return inca.form.metadata[table].regvars[regvar].label;
                    }
                }
            };

            self.getText = function(data) {
                if(!data.text && self.table().length && self.table()[0][data.field].rcc && self.table()[0][data.field].rcc.regvar)
                    return self.table()[0][data.field].rcc.regvar.label;
                else if(self.isRegisterRecord)
                    return self.getMetadataLabel(data.field);
                else 
                    return data.text; 
            };

            self.helpPopover = function(obj) {
                return ko.computed(function() {
                    var helpText = self.table()[0] && self.table()[0][obj.field].rcc && self.table()[0][obj.field].rcc.documentation && self.table()[0][obj.field].rcc.documentation() && self.table()[0][obj.field].rcc.documentation().helpText;
          
                    return helpText ? {
                            title: self.table()[0][obj.field].rcc.regvar.label,
                            content:  helpText,
                            trigger: 'hover',
                            html: true
                        } : null;
            })};

            if (params.baseQuestion) {
                self.baseQuestion.rcc = {
                    include: ko.observable(false),
                    term: {
                        dataType: 'list',
                        listValues: [{
                            text: 'Nej',
                            id: 1
                        }, {
                            text: 'Ja',
                            id: 2
                        }]
                    },
                    regvar: {
                        label: params.baseQuestion
                    },
                    accessed: ko.observable(),
                    validation: {
                        add: function () {},
                        errors: ko.observableArray()
                    },
                    dependenciesFulfilled: ko.observable(true)
                };

                if (self.table.length) {
                    self.baseQuestion(self.baseQuestion.rcc.term.listValues[1]);
                }
                self.table.subscribe(function (val) {
                    if (val && val.length) {
                        self.baseQuestion(self.baseQuestion.rcc.term.listValues[1]);
                    } else if (self.baseQuestion() && self.baseQuestion().id == 2) {
                        self.baseQuestion(null);
                    }

                });

                self.baseQuestion.subscribe(function (newValue) {
                    if (newValue && newValue.id === 2) {
                        if (!self.table().length) {
                            self.table.rcc.add();
                        }
                    } else {
                        self.table().forEach(function (row) {
                            self.table.rcc.remove(row);
                        });
                    }
                });
            }
        },
        template: RCC.Vast.Utils.extractSource(function () {/**@preserve
         <!-- ko if: $component.table -->
         <!-- ko if: $component.baseQuestion.rcc -->
            <s-a params="value: $component.baseQuestion, question: baseQuestion.rcc.regvar.label, showReviewerInclude: false"></s-a>
         <!-- /ko -->
         <!-- ko if: $component.table().length -->
             <table class="table table-condensed table-multi-answer-question" data-bind="css: { monitor: isReviewer }">
                <thead>
                    <tr>
                        <!-- ko if: $component.expandedContent.length -->
                            <th style="width: 40px"></th>
                        <!-- /ko -->
                        <!-- ko foreach: $component.columns -->
                            <th data-bind="css: $data.class">
                                <span class="question" data-bind="popover: $parent.helpPopover($data), html: $component.getText($data) + ($parent.helpPopover($data)() ? ' <i class=\'fa fa-info-circle\'></i>' : '')"></span>
                            </th>
                        <!-- /ko -->
                        <th></th>
                    </tr>
                </thead>
                <tbody>
                    <!-- ko foreach: { data: $component.table, as: 'tablerow' } -->
                        <tr>
                            <!-- ko if: $component.expandedContent.length -->
                                <td style="width: 40px">
                                    <i  class="fa"
                                        style="cursor: pointer"
                                        data-bind="css: {
                                            'fa-plus-circle': !tablerow.isExpanded(),
                                            'fa-minus-circle': tablerow.isExpanded()
                                        },
                                        click: tablerow.toggleExpand"></i>
                                </td>
                            <!-- /ko -->
                            <!-- ko foreach: $component.columns -->
                                <td data-bind="css: $data.class">
                                    <s-a params="value: tablerow[$data.field], controlType: $data.isStatic ? 'staticText' : undefined"></s-a>
                                    <!-- /ko -->
                                </td>
                            <!-- /ko -->
                                <td data-bind="if: $component.table().length > $component.minRow && ($component.canRemoveExistingRows || !tablerow.id)">
                                    <button type="button" class="btn btn-danger btn-xs" data-bind="enable: inca.errand, click: function () { $component.table.rcc.remove(tablerow) }">Ta bort</button>
                                </td>
                        </tr>
                         <!-- ko if: tablerow.isExpanded -->


                                 <tr class="subRow expandedContent">
                                     <td data-bind="attr: { colspan: $component.fullColspan}">
         <!-- ko foreach: { data: $component.expandedContent, as: 'expandedRow' } -->
                                        <!-- ko if: expandedRow.regvar -->
                                            <s-a params="value: tablerow[expandedRow.regvar], question: expandedRow.question, controlType: expandedRow.isStatic ? 'staticText' : undefined"></s-a>
                                        <!-- /ko -->

                                        <!-- ko if: expandedRow.component -->
                                            <div data-bind="component: { name: expandedRow.component, params: expandedRow.getParams(tablerow) }"></div>
                                        <!-- /ko -->
         <!-- /ko -->
                                     </td>
                                 </tr>


                         <!-- /ko -->
                        <!-- ko foreach: { data: $parent.extraRows, as: 'extrarow' } -->
                            <!-- ko if: !$data.condition || $data.condition(tablerow) -->
                                <!-- ko foreach: { data: tablerow.$$[extrarow.subTable](), as: 'subrow' } -->
                                    <!-- ko foreach: extrarow.questions -->
                                        <tr class="subRow">
                                            <td data-bind="attr: { colspan: $component.fullColspan}">
                                                <s-a params="value: subrow[$data.regvar], question: $data.question"></s-a>
                                            </td>
                                        </tr>
                                    <!-- /ko -->
                                <!-- /ko -->
                            <!-- /ko -->
                        <!-- /ko -->
                        <!-- ko foreach: { data: $component.components, as: 'subcomponent' } -->
                            <tr class="subRow">
                                <td data-bind="attr: { colspan: $component.fullColspan}">
                                    <div data-bind="component: { name: subcomponent.name, params: subcomponent.getParams(tablerow) }"></div>
                                 </td>
                             </tr>
                        <!-- /ko -->
                    <!-- /ko -->
                </tbody>
             </table>
         <!-- /ko -->
         <!-- ko if: (!$component.baseQuestion.rcc || $component.table().length) && $component.table.rcc && $component.table.rcc.add && !inca.form.isReadOnly -->
             <div class="r">
                <button type="button" class="btn btn-primary btn-xs" data-bind="click: function() { $component.table.rcc.add(); }, text: $component.addRowText"></button>
             </div>
         <!-- /ko -->
         <!-- /ko -->
         */
            return undefined;
        })

    });
})(window['inca'], window['RCC'], window['ko'], window['_']);