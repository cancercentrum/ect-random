/**
* Dialog
* Användning:
*	<m-dialog params="@param: värde"></m-dialog>
*
* @param {String} [title] Rubrik för dialog.
* @param {String|ko.template} [body] HTML för dialog.
* @param {String} [cssClass] CSS-klasser.
* @param {Boolean|ko.subscribable} [visible=true] Hanterar visning av dialog. Boolean-värden skrivs om till ko.observable.
* @param {Object} [primaryButton] Standardknapp.
* @param {String} [primaryButton.text='Spara'] Knapptext.
* @param {function(Object)} [primaryButton.click] Event för klick. Standardeventet döljer dialogen. Vymodellen finns tillgänglig som argument.
* @param {Boolean|ko.subscribable} [primaryButton.visible=true] Hanterar visning av knapp. Boolean-värden skrivs om till ko.observable.
* @param {Boolean|ko.subscribable} [primaryButton.enabled=true] Anger om knappen ska vara klickbar. Boolean-värden skrivs om till ko.observable.
* @param {String} [primaryButton.cssClass='btn-primary'] CSS-klasser för knapp.
* @param {Object} [secondaryButton] Sekundär knapp.
* @param {String} [secondaryButton.text='Avbryt'] Se primaryButton.text.
* @param {function(Object)} [secondaryButton.click] Se primaryButton.click.
* @param {Boolean|ko.subscribable} [secondaryButton.visible=true] Se primaryButton.visible.
* @param {Boolean|ko.subscribable} [secondaryButton.enabled=true] Se primaryButton.enabled.
* @param {String} [secondaryButton.cssClass='btn-default'] Se primaryButton.cssClass.
* @param {Object} [closeButton=secondaryButton] Stäng-knapp. Använder secondaryButton som standardvärde.
* @param {String} [closeButton.text] Title-attribut för knapp.
* @param {function(Object)} [closeButton.click] Se primaryButton.click.
* @param {Boolean|ko.subscribable} [closeButton.visible=true] Se primaryButton.visible.
*
* 
* Användning:
*	RCC.Vast.Components.Dialog.createDialog(Object, Object)
*
* @param {Object} componentParams Parametrar för <m-dialog>.
* @param {Object} [position] Positionering av dialog.
* @param {String|Object} [position.relativeTo] Positionerar dialog relativt till ett element. Elementet skickas in med CSS-selektor eller annan typ som kan tolkas av jQuery.
* @param {Number} [position.top] Positionering topp (px).
* @param {Number} [position.left] Positionering vänster (px).
* @returns {Object} Vymodell.
*/

(function(RCC, ko, _, $) {
	if (!RCC.Vast.Components) RCC.Vast.Components = {};
	RCC.Vast.Components.Dialog = {
		createDialog: function(componentParams, position) {
			var getPosition = (function() {
				if (position) {
					_.defaults(position, { top: 0, left: 10 });
					if (position.relativeTo) return function() {
						var relativeTo = $(position.relativeTo).offset();
						return { top: relativeTo.top + position.top, left: relativeTo.left + position.left };
					};
					return function() { return position; };
				}

			 	return function() {
					return { left: 10, top: ($(parent).scrollTop() + 200) };
				};
			})();

			var el = $('<div></div>').addClass('create-dialog').appendTo('body'),
				v = componentParams.visible;

			componentParams.visible = (ko.isWritableObservable(v) ? v : (ko.observable(_.isBoolean(v) ? v : true)));

			function setElementCss(val) {
				if (val) {
					var pos = getPosition();
					el.css('left', pos.left).css('top', pos.top);
				}
			}
			componentParams.visible.subscribe(setElementCss);
			setElementCss(componentParams.visible());

			ko.applyBindingsToNode(el[0], { component: { name: 'm-dialog', params: componentParams } });
			return componentParams;
		}
	};
})(window['RCC'], window['ko'], window['_'], window['jQuery']);

(function(RCC, ko, _) {
	ko.components.register('m-dialog', {
		synchronous: !RCC.Vast.Utils.ieVersion || RCC.Vast.Utils.ieVersion > 8,
		viewModel: function(params) {
			function boolFunc(x) { return (_.isObject(x) ? x : (ko.observable(_.isBoolean(x) ? x : true))); };
			function DialogButton(opt) {		
				var m = this;
				m.text = opt.text;
				m.click = (opt.click ? function() { return opt.click.apply(this, [params].concat(_.toArray(arguments))); } : function() { params.visible(false); });
				m.visible = boolFunc(opt.visible);
				m.enabled = boolFunc(opt.enabled);
				m.cssClass = opt.cssClass;
			}

			_.defaults(params, { title: '', body: '', cssClass: '' });
			params.visible = boolFunc(params.visible);
			params.primaryButton = new DialogButton(_.defaults(params.primaryButton || {}, { text: 'Spara', cssClass: 'btn-primary' }));
			params.secondaryButton = new DialogButton(_.defaults(params.secondaryButton || {}, { text: 'Avbryt', cssClass: 'btn-default' }));
			params.closeButton = (params.closeButton ? new DialogButton(params.closeButton) : params.secondaryButton);

			return params;
		},
		template: RCC.Vast.Utils.extractSource(function() {/**@preserve
			<div data-bind="visible: visible" class="rcc-modal" tabindex="-1">
                <div class="rcc-modal-overlay"></div>
                <div data-bind="attr: { 'class': 'modal-dialog ' + cssClass }">
                    <div class="modal-content">
                        <div class="modal-header">
                        	<!-- ko if: closeButton.visible() -->
								<button data-bind="attr: { title: closeButton.text }, click: closeButton.click" class="close">&times;</button>
                        	<!-- /ko -->
                            <h4 class="modal-title" data-bind="text: title"></h4>
                        </div>
                  		<!-- ko if: _.isString(body) -->
							<div class="modal-body" data-bind="html: body"></div>
                  		<!-- /ko -->
                  		<!-- ko if: _.isObject(body) -->
							<div class="modal-body" data-bind="template: body"></div>
                  		<!-- /ko -->
                        <div class="modal-footer" data-bind="foreach: [secondaryButton, primaryButton]">
                        	<!-- ko if: visible() -->
								<button type="button" data-bind="attr: { 'class': 'btn ' + cssClass }, enable: enabled(), click: click, text: text"></button>
                        	<!-- /ko -->
                        </div>
                    </div>
                </div>
            </div>
		*/return undefined;})
	});
})(window['RCC'], window['ko'], window['_']);