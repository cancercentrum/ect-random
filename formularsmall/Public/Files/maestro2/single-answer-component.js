/**
* Hanterar frågor med 1 svarsalternativ.
*
* @param {ko.observable} value Datakoppling för kontroll.
* @param {String} [question] Frågetext.
* @param {String} [info] Frågans hjälptext.
* @param {String|Object} [controlType] Typ av kontroll, 'select', 'text', 'datepicker', 'staticText', 'decimal', 'textarea' eller 'checkbox'. Sätts automatiskt från datakopplingens datatyp om ej parameter anges.
* @param {String} [controlType.name] Typ av kontroll.
* @param {Object} [controlType.options] Argument för datakoppling. Använder standardvärden för de argument som inte anges.
* @param {function(Object)} [controlChange] change-event för kontroll. $parent finns tillgänglig som argument.
* @param {String} [placeholder] Text som visas efter kontroll.
* @param {Boolean} [fullWidthText=false] Sätter full bredd på frågetext och hjälptext.
* @param {Boolean} [showReviewerInclude=true] Visar monitorkryssruta om övriga villkor är uppfyllda.
* @param {String} [tooltip] Registervariabelns tooltip.
*/
(function(inca, RCC, ko, _) {
	ko.components.register('s-a', {
		synchronous: !RCC.Vast.Utils.ieVersion || RCC.Vast.Utils.ieVersion > 8,
		viewModel: function(params) {
			if (!params.value && params.controlType != 'staticText') throw new Error('Parameter "value" is required.');
			var self = this;
			_.extend(self, _.omit(_.defaults(params, {
				question: params.value.rcc.regvar.label,
				info: '',
				controlType: { datetime: 'datepicker', list: 'select', boolean: 'checkbox' }[params.value.rcc && params.value.rcc.term.dataType] || 'text',
				fullWidthText: false,
				placeholder: '',
				enabled: true,
				showReviewerInclude: true,
				tooltip: params.value.rcc.regvar.description,
				dependenciesFulfilled: params.value.rcc.dependenciesFulfilled,
				optionsText: 'text',
                optionsCaption: '– Välj –',
                updateOnKeyPress: false
			}), 'controlType', 'controlChange'));

            self.helpPopover = ko.computed(function() {
                var helpText = params.help || (params.value.rcc && params.value.rcc.documentation && params.value.rcc.documentation() && params.value.rcc.documentation().helpText);
                return helpText ? {
                        title: params.value.rcc.regvar.label,
                        content:  helpText,
                        trigger: 'hover',
                        html: true
                    } : null;
            });

			if (self.showReviewerInclude)
				self.showReviewerInclude = !!(inca.user.role.isReviewer && !inca.form.isReadOnly && self.value.rcc && self.enabled);

		 	self.control = new function() {
				var m = this;
				m.type = (typeof params.controlType == 'string' ? params.controlType : params.controlType.name);
				m.options = _.defaults(params.controlType.options || {}, (function() {
					switch (m.type) {
						case 'datepicker': return { value: self.value, dateToday: inca.serverDate, readOnly: inca.form.isReadOnly || !self.enabled };
						case 'decimal': return { valueObservable: self.value };
						case 'select': return { value: self.value, caption: self.optionsCaption, text: self.optionsText };
						case 'staticText':
							if( _.isFunction(self.value) && _.isObject(self.value()) && self.value().text ){
								return { value: self.value().text };
							}
							return { value: self.value };
						default: return { value: self.value };
					}
				})());
                m.toggleInclude = function () {
                    self.value.rcc.include(!self.value.rcc.include());
                };
		 	};

			self.createControlEvent = function($parent) {
				return new function() {
					if (params.controlChange) this.change = function() { return params.controlChange($parent); };
				};
			};

			self.hasErrors = ko.computed(function () {
				var errors = params.value && params.value.rcc && params.value.rcc.validation && params.value.rcc.validation.errors();
				return errors && errors.length;
			});

			self.hasFatalErrors = ko.computed(function () {
				var errors = params.value && params.value.rcc && params.value.rcc.validation && params.value.rcc.validation.errors();
				return !!(errors && errors.filter(function (e) { return e.fatal; }).length);
			});
		},
		template: RCC.Vast.Utils.extractSource(function() {/**@preserve
			<!-- ko if: dependenciesFulfilled -->
				<div class="single-answer-template r" data-bind="attr: { title: tooltip }">
					<label data-bind="css: { 'accessed': value.rcc && value.rcc.accessed(), 'errors': hasErrors, 'fatal': hasFatalErrors }">
						<span data-bind="popover: helpPopover, html: question + (helpPopover() ? ' <i class=\'fa fa-info-circle\'></i>' : '') + (info ? '<br /><span class=\'help\'>' + info + '</span>' : ''), css: { 'full-width-text': fullWidthText }" class="question"></span>
						<!-- ko if: ko.utils.unwrapObservable($root.showRegvarNames) && value.rcc -->
							<span class="regvar-name" data-bind="text: value.rcc.regvarName"></span>
						<!-- /ko -->
						<!-- ko with: _.extend(control, { event: createControlEvent($parent) }) -->
							<!-- ko if: type == 'select' -->
								<select class="ctrl" data-bind="enable: $parent.enabled, rcc-list: options.value, optionsCaption: options.caption, event: event, optionsText: options.text"></select>
							<!--/ko-->
							<!-- ko if: type == 'text' -->
								 <!-- ko if: $component.updateOnKeyPress -->
								 	<input class="ctrl" type="text" data-bind="rcc-textInput: options.value, event: event, enable: $parent.enabled" />
								 <!--/ko-->
								 <!-- ko ifnot: $component.updateOnKeyPress -->
								 	<input class="ctrl" type="text" data-bind="rcc-value: options.value, event: event, enable: $parent.enabled" />
								 <!--/ko-->
							<!--/ko-->
							<!-- ko if: type == 'datepicker' -->
								<input class="ctrl" type="text" data-bind="rcc-datepicker: options, event: event, enable: $parent.enabled" />
							<!--/ko-->
							<!-- ko if: type == 'staticText' -->
								<!-- ko text: options.value --><!-- /ko -->
							<!--/ko-->
							<!-- ko if: type == 'decimal' -->
								<input class="ctrl" type="text" data-bind="rcc-decimal: options, event: event, enable: $parent.enabled" />
							<!--/ko-->
							<!-- ko if: type == 'textarea' -->
								<textarea class="ctrl" type="text" data-bind="rcc-value: options.value, event: event, enable: $parent.enabled" rows="5"></textarea>
							<!--/ko-->
							<!-- ko if: type == 'checkbox' -->
								<input type="checkbox" data-bind="rcc-checked: options.value, event: event, enable: $parent.enabled">
							<!--/ko-->
							<!-- ko if: $parent.showReviewerInclude -->
								<span class="reviewer-include" title="Inkludera">
                                    <i class="glyphicon" data-bind="click: toggleInclude, css: { 'glyphicon-unchecked': !options.value.rcc.include(), 'glyphicon-check': options.value.rcc.include() }"></i>
								</span>
							<!--/ko-->
						<!--/ko-->
						<!-- ko if: placeholder -->
							<!-- ko text: placeholder --><!--/ko-->
						<!--/ko-->

						<!-- ko if: hasErrors -->
						<div class="error-list" data-bind="foreach: value.rcc.validation.errors" >
							<span class="error-item" data-bind="css: { 'non-fatal': !fatal, 'fatal': fatal }">
								<span class="fa" data-bind="css: { 'fa-exclamation-triangle': !fatal, 'fa-exclamation-circle': fatal }"></span>
								<span data-bind="text: message"></span>
							</span>
						</div>
						<!--/ko-->

					</label>
				</div>
			<!--/ko-->
		*/return undefined;})
	});
})(window['inca'], window['RCC'], window['ko'], window['_']);
